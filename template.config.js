module.exports = {
  // Placeholder used to rename and replace in files
  // package.json, index.json, android/, ios/
  placeholderName: "DemoLeadTracker",

  // Directory with template
  templateDir: "./DemoLeadTracker",

  // Path to script, which will be executed after init
  postInitScript: "./script.js"
};
