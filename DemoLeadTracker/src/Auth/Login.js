import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, StatusBar, ScrollView, Image, Dimensions, TouchableOpacity, Keyboard, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import SnackBar from '../Component/SnackBar';
import Global from '../Global';
import { API } from '../Privet';

const { height, width } = Dimensions.get("window");

const RN = Platform.OS === 'ios' ? require('react-native') : require('react-native-safe-area-context');
const { SafeAreaView } = RN;

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            username: '',
            password: '',
        };
        SplashScreen.hide();
    }

    login() {
        const { username, password } = this.state;
        Keyboard.dismiss();

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!username) {
            this.refs.SnackBar.show('Please enter email address or mobile no.');
        }
        // else if (!reg.test(username)) {
        //     this.refs.SnackBar.show("Please enter valid email address.");
        // }
        else if (!password) {
            this.refs.SnackBar.show("Please enter password.");
        }
        else {
            this.setState({ isLoading: true });
            const data = {
                emailid: username,
                password: password,
                dtoken: Global.main.state.deviceToken
            }

            // console.log(JSON.stringify(data))

            fetch(API.login, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    if (res.status) {
                        const items = [
                            ['id', res.data.id],
                            ['unique_id', res.data.unique_id],
                            ['fname', res.data.fname],
                            ['lname', res.data.lname],
                            ['profile_img', res.data.profile_img],
                            ['mobile', res.data.mobile],
                            ['emailid', res.data.emailid],
                            ['country_id', res.data.country_id],
                            ['country', res.data.country],
                            ['state_id', res.data.state_id],
                            ['city_id', res.data.city_id],
                            ['state', res.data.state],
                            ['city', res.data.city],
                            ['urole', res.data.urole],
                            ['urole_name', res.data.urole_name]
                        ]
                        AsyncStorage.multiSet(items, async () => {
                            await Global.main.setState({
                                id: res.data.id,
                                unique_id: res.data.unique_id,
                                fname: res.data.fname,
                                lname: res.data.lname,
                                profile_img: res.data.profile_img,
                                mobile: res.data.mobile,
                                emailid: res.data.emailid,
                                country_id: res.data.country_id,
                                country: res.data.country,
                                state_id: res.data.state_id,
                                city_id: res.data.city_id,
                                state: res.data.state,
                                city: res.data.city,
                                urole: res.data.urole,
                                urole_name: res.data.urole_name
                            });
                            await this.setState({ isLoading: false }, () =>
                                this.props.navigation.navigate('Home')
                            );
                        })

                    } else {
                        this.refs.SnackBar.show(res.message);
                        this.setState({ isLoading: false });
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }

        // this.props.navigation.navigate('Home')
        // AsyncStorage.setItem('username', "Manish Modha");
        // Global.main.setState({ username: "Manish Modha" });
    }

    render() {
        const { isLoading, keyboardShow, username, password } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }} >
                <StatusBar translucent backgroundColor="#fff0" barStyle="light-content" />
                {/* <SafeAreaView /> */}
                <Image source={require('../assets/img/login_footer.png')} style={{ position: 'absolute', bottom: 0, width: '100%', height: 150, resizeMode: 'stretch', tintColor: color.primeColor }} />
                <View style={{ height: height * .45, width: '100%', backgroundColor: color.primeColor, position: 'absolute', justifyContent: 'center', alignItems: 'center' }} >
                    {/* <Text style={{ paddingHorizontal: 15, fontSize: 40, fontFamily: font.bold, color: '#fff', textAlign: 'center', marginBottom: 40 }}>LOGO</Text> */}
                    <Image source={require('../assets/img/logo.png')} style={{ height: '40%', marginTop: -height * .09, width: '80%', resizeMode: 'contain', tintColor: '#fff' }} />
                </View>

                {/* <View style={{ height: height * .35, width: '100%', position: 'absolute', justifyContent: 'center', alignItems: 'center' }} >
                    <Image source={require('../assets/img/login_footer.png')} style={{ position: 'absolute', width: '100%', height: 150, top: 0, resizeMode: 'stretch', tintColor: color.primeColor, transform: [{ rotate: '180deg' }] }} />
                    <Image source={require('../assets/img/logo.png')} style={{ height: 100, width: '70%', resizeMode: 'contain', }} />
                </View> */}

                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? "padding" : null}>

                    <ScrollView bounces={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>

                        <View style={{ paddingBottom: 35 / 2 }}>
                            <View style={{ ...shadow, paddingVertical: 20, paddingHorizontal: 5, borderRadius: 8, margin: 20 }}>

                                <Text style={{ paddingHorizontal: 15, fontSize: 30, fontFamily: font.bold, marginBottom: 20, color: color.primeColor, textAlign: 'center' }}>LOGIN</Text>

                                <View style={{ marginBottom: 15 }}>
                                    <FloatingLabelInput
                                        autoCapitalize="none"
                                        keyboardType="email-address"
                                        label="Email or Mobile"
                                        value={username}
                                        onChangeText={username => this.setState({ username })}
                                    />
                                </View>

                                <View style={{ marginBottom: 30, }}>
                                    <FloatingLabelInput
                                        autoCapitalize="none"
                                        label="Password"
                                        secureTextEntry
                                        value={password}
                                        onChangeText={password => this.setState({ password })}
                                    />
                                </View>

                                {/* <View style={{ marginHorizontal: 15, alignItems: 'flex-end', marginVertical: 20 }}>
                                <TouchableOpacity>
                                    <Text style={{ fontFamily: font.semibold, fontSize: 13 }}>FORGET PASSWORD?</Text>
                                </TouchableOpacity>
                            </View> */}

                            </View>
                            <TouchableOpacity activeOpacity={.98} onPress={() => this.login()} disabled={isLoading} style={{ ...shadow, position: 'absolute', left: 15, right: 15, bottom: 35 / 2, marginHorizontal: 15, backgroundColor: color.primeColor, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 8 }}>
                                {isLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, fontSize: 15, color: '#fff' }}>LOGIN</Text>}
                            </TouchableOpacity>

                        </View>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')} style={{ height: 35, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', ...shadow, borderRadius: 35, marginTop: 10, marginBottom: 20, flexDirection: 'row', paddingRight: 5 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor, paddingLeft: 30, paddingRight: 20, textAlign: 'center' }}>Register Now!</Text>
                            <View style={{ height: 30, width: 30, backgroundColor: color.primeColor, borderRadius: 35, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/img/back.png')} style={{ height: '45%', width: '45%', resizeMode: 'contain', tintColor: '#fff', transform: [{ rotate: '180deg' }] }} />
                            </View>
                        </TouchableOpacity>
                    </ScrollView>
                </KeyboardAvoidingView>
                <SnackBar ref="SnackBar" />
            </View>
        );
    }
}
