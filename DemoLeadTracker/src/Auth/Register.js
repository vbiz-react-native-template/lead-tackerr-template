import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, StatusBar, ScrollView, Image, Dimensions, TouchableOpacity, Keyboard, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import Picker from '../Component/Picker';
import ImagePicker from '../Component/ImagePicker';
import SnackBar from '../Component/SnackBar';
import Global from '../Global';
import { API } from '../Privet';

const { height, width } = Dimensions.get("window");

const RN = Platform.OS === 'ios' ? require('react-native') : require('react-native-safe-area-context');
const { SafeAreaView } = RN;

export default class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            fname: '',
            lname: '',
            mobile: '',
            emailid: '',
            password: '',
            country: [],
            state: [],
            city: [],
            profile_img: [],

            countryList: [],
            stateList: [],
            cityList: [],
        };
        SplashScreen.hide();
    }

    componentDidMount() {
        this.countryList();
    }

    register() {
        const { fname, lname, mobile, emailid, city, country, state, password, profile_img } = this.state;
        Keyboard.dismiss();

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!fname) {
            this.refs.SnackBar.show('Please enter first name.');
        }
        else if (!lname) {
            this.refs.SnackBar.show('Please enter last name.');
        }
        else if (!mobile) {
            this.refs.SnackBar.show('Please enter mobile number.');
        }
        else if (mobile.length != 10) {
            this.refs.SnackBar.show('Please enter velide mobile number.');
        }
        else if (!emailid) {
            this.refs.SnackBar.show('Please enter email address.');
        }
        else if (!reg.test(emailid)) {
            this.refs.SnackBar.show("Please enter valid email address.");
        }
        else if (!country.id) {
            this.refs.SnackBar.show('Please select country.');
        }
        else if (!state.id) {
            this.refs.SnackBar.show('Please select state.');
        }
        else if (!city.id) {
            this.refs.SnackBar.show('Please select city.');
        }
        else if (!password) {
            this.refs.SnackBar.show("Please enter password.");
        }
        else {
            this.setState({ isLoading: true });
            let formdata = new FormData();

            formdata.append('fname', fname);
            formdata.append('lname', lname);
            formdata.append('mobile', mobile);
            formdata.append('emailid', emailid);
            formdata.append('password', password);
            formdata.append('country_id', country.id);
            formdata.append('state', state.id);
            formdata.append('city', city.id);
            formdata.append('profile_img', profile_img.path ? { uri: profile_img.path, name: profile_img.filename, type: profile_img.mime } : '');

            // console.log(JSON.stringify(data))

            fetch(API.employee_register, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formdata
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    if (res.status) {
                        this.setState({ isLoading: false }, () =>
                            this.props.navigation.goBack()
                        );

                    } else {
                        this.refs.SnackBar.show(res.message);
                        this.setState({ isLoading: false });
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    // countryList
    countryList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.country_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res)
                if (res.status) {
                    this.setState({ countryList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // stateList
    stateList(country_id) {
        const { id, urole } = Global.main.state;
        const data = {
            // user_id: id,
            // utype: urole,
            country_id: country_id.id
        }

        // console.log(data)

        fetch(API.state_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('stateList: ', res)
                if (res.status) {
                    this.setState({ stateList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // cityList
    cityList(state_id) {
        const { id, urole } = Global.main.state;
        const data = {
            // user_id: id,
            // utype: urole,
            state_id: state_id.id
        }

        fetch(API.city_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('ciryList: ', res)
                if (res.status) {
                    this.setState({ cityList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    render() {
        const { isLoading, keyboardShow, fname, lname, mobile, emailid, password, city, country, state, countryList, stateList, cityList, profile_img } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }} >
                <StatusBar translucent backgroundColor="#fff0" barStyle="light-content" />
                {/* <SafeAreaView /> */}
                <Image source={require('../assets/img/login_footer.png')} style={{ position: 'absolute', bottom: 0, width: '100%', height: 150, resizeMode: 'stretch', tintColor: color.primeColor }} />
                <View style={{ height: height * .45, width: '100%', backgroundColor: color.primeColor, position: 'absolute', justifyContent: 'center', alignItems: 'center' }} >
                    {/* <Text style={{ paddingHorizontal: 15, fontSize: 40, fontFamily: font.bold, color: '#fff', textAlign: 'center', marginBottom: 40 }}>LOGO</Text> */}
                    <Image source={require('../assets/img/logo.png')} style={{ height: '40%', marginTop: -height * .09, width: '80%', resizeMode: 'contain', tintColor: '#fff' }} />
                </View>

                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? "padding" : null}>

                    <ScrollView bounces={false} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>

                        <View style={{ paddingBottom: 35 / 2 }}>
                            <View style={{ ...shadow, paddingVertical: 20, paddingHorizontal: 5, borderRadius: 8, margin: 20 }}>

                                <Text style={{ paddingHorizontal: 15, fontSize: 30, fontFamily: font.bold, marginBottom: 20, color: color.primeColor, textAlign: 'center' }}>REGISTER</Text>

                                <View style={{ height: 70, width: 70, borderRadius: 70, borderWidth: 1, alignSelf: 'center', marginBottom: 15 }}>
                                    <TouchableOpacity onPress={() => this.refs.ImagePicker.show("profile")} style={{ height: 70, width: 70, borderRadius: 70, justifyContent: 'center', alignItems: 'center' }}>
                                        {
                                            !profile_img.path ?
                                                <Image source={require('../assets/img/camera_icon.png')} style={{ height: '45%', width: '45%', resizeMode: 'contain' }} />
                                                :
                                                <Image source={{ uri: profile_img.path }} style={{ height: '100%', width: '100%', borderRadius: 70 }} />
                                        }
                                    </TouchableOpacity>
                                </View>

                                <View style={{ marginBottom: 15, flexDirection: 'row', alignItems: 'center' }}>
                                    <FloatingLabelInput
                                        viewStyle={{ flex: 1, }}
                                        autoCapitalize="words"
                                        label="First Name"
                                        value={fname}
                                        onChangeText={fname => this.setState({ fname })}
                                    />

                                    <FloatingLabelInput
                                        viewStyle={{ flex: 1, marginLeft: 0 }}
                                        autoCapitalize="words"
                                        label="Last Name"
                                        value={lname}
                                        onChangeText={lname => this.setState({ lname })}
                                    />
                                </View>

                                <View style={{ marginBottom: 15 }}>
                                    <FloatingLabelInput
                                        keyboardType="number-pad"
                                        label="Mobile"
                                        value={mobile}
                                        maxLength={10}
                                        onChangeText={mobile => this.setState({ mobile })}
                                    />
                                </View>

                                <View style={{ marginBottom: 15 }}>
                                    <FloatingLabelInput
                                        autoCapitalize="none"
                                        keyboardType="email-address"
                                        label="Email"
                                        value={emailid}
                                        onChangeText={emailid => this.setState({ emailid })}
                                    />
                                </View>

                                <View style={{ marginBottom: 15 }}>
                                    <Picker
                                        placeholder="Country"
                                        data={countryList}
                                        searchBar
                                        value={country.name}
                                        onSelect={_country => _country.id != country.id && this.setState({ country: _country, status: [], city: [], stateList: [], cityList: [], }, () => this.stateList(_country))}
                                    />
                                </View>

                                <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                    <Picker
                                        placeholder="State"
                                        mainViewStyle={{ marginRight: 15, flex: 1, }}
                                        data={stateList}
                                        searchBar
                                        nameKey="sname"
                                        value={state.sname}
                                        onSelect={_state => _state.id != state.id && this.setState({ state: _state, city: [], cityList: [], }, () => this.cityList(_state))}
                                    />
                                    <Picker
                                        placeholder="City"
                                        mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                        data={cityList}
                                        searchBar
                                        nameKey="cname"
                                        value={city.cname}
                                        onSelect={city => this.setState({ city })}
                                    />
                                </View>

                                <View style={{ marginBottom: 30, }}>
                                    <FloatingLabelInput
                                        autoCapitalize="none"
                                        label="Password"
                                        secureTextEntry
                                        value={password}
                                        onChangeText={password => this.setState({ password })}
                                    />
                                </View>

                            </View>
                            <TouchableOpacity activeOpacity={.98} onPress={() => this.register()} disabled={isLoading} style={{ ...shadow, position: 'absolute', left: 15, right: 15, bottom: 35 / 2, marginHorizontal: 15, backgroundColor: color.primeColor, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 8 }}>
                                {isLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, fontSize: 15, color: '#fff' }}>REGISTER</Text>}
                            </TouchableOpacity>

                        </View>

                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ height: 35, alignSelf: 'center', justifyContent: 'center', alignItems: 'center', ...shadow, borderRadius: 35, marginTop: 10, flexDirection: 'row', paddingLeft: 4, marginBottom: 20 }}>
                            <View style={{ height: 30, width: 30, backgroundColor: color.primeColor, borderRadius: 35, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/img/back.png')} style={{ height: '45%', width: '45%', resizeMode: 'contain', tintColor: '#fff', }} />
                            </View>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor, paddingLeft: 20, paddingRight: 30, textAlign: 'center' }}>Login Now!</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </KeyboardAvoidingView>
                <SnackBar ref="SnackBar" />
                <ImagePicker ref="ImagePicker" onSelect={(profile_img) => this.setState({ profile_img })} />
            </View>
        );
    }
}
