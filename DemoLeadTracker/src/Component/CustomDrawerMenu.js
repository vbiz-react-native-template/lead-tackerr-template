import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, Modal, SafeAreaView, Share, TouchableWithoutFeedback, Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
// import { SafeAreaView } from 'react-native-safe-area-context';

import { font, color, shadow } from './Styles';
import CustomAlert from './CustomAlert';
import { User } from '../Privet';
import Global from '../Global';

export default class CustomDrawerMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logoutVisible: false
        };
    }

    componentDidMount() {
        // console.log(this.props)
    }

    onPress(key) {
        // alert(key)
        if (key) {
            // User.active_menu = key;
            this.props.navigation.navigate(key);
            this.props.navigation.closeDrawer();
        }
    }

    async logout() {
        // alert('hello')
        await AsyncStorage.clear();
        await AsyncStorage.removeItem('id');

        await this.props.navigation.closeDrawer();
        await Global.main.setState({ id: '' });
        // await this.setState({ logoutVisible: false });
        // await this.props.navigation.navigate('Login');
    }

    render() {
        const { logoutVisible } = this.state;
        const { fname, lname, urole_name, profile_img } = Global.main.state;

        const routes = this.props.state.routes[0].state ? this.props.state.routes[0].state.routes : null;

        const activeRouteName = routes ? routes[routes.length - 1].name : 'Home';

        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <SafeAreaView style={{ backgroundColor: '#fff' }} />

                <View style={{ padding: 15, paddingVertical: 30, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ height: 55, width: 55, justifyContent: 'center', alignItems: 'center', marginRight: 15 }}>
                        {/* <Image source={require('../assets/img/circle_icon.png')} style={{ height: '100%', width: '100%', resizeMode: 'contain', position: 'absolute' }} /> */}
                        <Image source={{ uri: profile_img }} style={{ height: '100%', width: '100%', borderRadius: 200 }} />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text numberOfLines={1} style={{ fontFamily: font.bold, fontSize: 15, }}>Hello, {fname} {lname}</Text>
                        <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3 }}>{urole_name}</Text>
                    </View>
                </View>

                <ScrollView bounces={false} contentContainerStyle={{ flexGrow: 1 }}>

                    <View style={{ flex: 1, }}>

                        {/* Home */}
                        <View borderColor={activeRouteName === 'Home' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('Home')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/home.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Home' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Home' ? color.primeColor : color.lableColor, fontSize: 16 }}>Dashboard</Text>
                            </TouchableOpacity>
                        </View>

                        {/* EmployeeDashboard */}
                        <View borderColor={activeRouteName === 'EmployeeDashboard' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('EmployeeDashboard')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/ratio.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'EmployeeDashboard' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'EmployeeDashboard' ? color.primeColor : color.lableColor, fontSize: 16 }}>Employee Dashboard</Text>
                            </TouchableOpacity>
                        </View>

                        {/* My Account */}
                        {/* <View borderColor={activeRouteName === 'MyAccount' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('MyAccount')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/user.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'MyAccount' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'MyAccount' ? color.primeColor : color.lableColor, fontSize: 16 }}>My Account</Text>
                            </TouchableOpacity>
                        </View> */}

                        {/* Lead */}
                        <View borderColor={activeRouteName === 'Lead' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('Lead')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/user-list.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Lead' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Lead' ? color.primeColor : color.lableColor, fontSize: 16 }}>Lead Management</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Today Follow Up */}
                        <View borderColor={activeRouteName === 'TodayFollowUp' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('TodayFollowUp')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/phone.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'TodayFollowUp' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'TodayFollowUp' ? color.primeColor : color.lableColor, fontSize: 16 }}>Today's Followups</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Pending Follow Up */}
                        <View borderColor={activeRouteName === 'PendingFollowUp' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('PendingFollowUp')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/phone-with-wire.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'PendingFollowUp' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'PendingFollowUp' ? color.primeColor : color.lableColor, fontSize: 16 }}>Pending Followups</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Customers */}
                        <View borderColor={activeRouteName === 'Customers' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('Customers')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/customer.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Customers' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Customers' ? color.primeColor : color.lableColor, fontSize: 16 }}>Customers</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Notification */}
                        <View borderColor={activeRouteName === 'Notification' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('Notification')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/notification.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Notification' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Notification' ? color.primeColor : color.lableColor, fontSize: 16 }}>Notification</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Support */}
                        {/* <View borderColor={activeRouteName === 'Support' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('Support')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/help.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Support' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Support' ? color.primeColor : color.lableColor, fontSize: 16 }}>Support</Text>
                            </TouchableOpacity>
                        </View> */}

                        {/* About Us */}
                        {/* <View borderColor={activeRouteName === 'AboutUs' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.onPress('AboutUs')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/about_us.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'AboutUs' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'AboutUs' ? color.primeColor : color.lableColor, fontSize: 16 }}>About Us</Text>
                            </TouchableOpacity>
                        </View> */}

                        {/* Logout */}
                        <View borderColor={activeRouteName === 'Logout' ? color.primeColor : '#fff'} style={{ borderLeftWidth: 3, marginBottom: 20 }}>
                            <TouchableOpacity onPress={() => this.refs.CustomAlert.onShow('Are you sure you want to logout!', 'Logout')} style={{ paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center', paddingVertical: 4 }}>
                                <Image source={require('../assets/img/logout.png')} style={{ height: 25, width: 25, resizeMode: 'contain', tintColor: activeRouteName === 'Logout' ? color.primeColor : color.lableColor }} />
                                <Text style={{ marginLeft: 20, flex: 1, fontFamily: font.reg, color: activeRouteName === 'Logout' ? color.primeColor : color.lableColor, fontSize: 16 }}>Logout</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                    <Text numberOfLines={1} style={{ fontFamily: font.reg, textAlign: 'center', paddingVertical: 15 }}>{DeviceInfo.getVersion()}</Text>

                    <CustomAlert ref="CustomAlert" onPressOk={() => this.logout()} cancel />
                </ScrollView>
            </View>
        );
    }
}
