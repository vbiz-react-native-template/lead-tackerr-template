import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import SplashScreen from 'react-native-splash-screen';
import OneSignal from 'react-native-onesignal';
import CustomDrawerMenu from './Component/CustomDrawerMenu';

import Global from './Global';

// Auth
import Login from './Auth/Login';
import Register from './Auth/Register';

// Screen
import Home from './Screen/Home';
import Lead from './Screen/Lead';
import AddLead from './Screen/AddLead';
import LeadDetails from './Screen/LeadDetails';
import TodayFollowUp from './Screen/TodayFollowUp';
import PendingFollowUp from './Screen/PendingFollowUp';
import Customers from './Screen/Customers';
import Notification from './Screen/Notification';
import EmployeeDashboard from './Screen/EmployeeDashboard';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function StateNavigation() {

    return (
        <Stack.Navigator initialRouteName="Home" headerMode="none">
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Lead" component={Lead} />
            <Stack.Screen name="AddLead" component={AddLead} />
            <Stack.Screen name="LeadDetails" component={LeadDetails} />
            <Stack.Screen name="TodayFollowUp" component={TodayFollowUp} />
            <Stack.Screen name="PendingFollowUp" component={PendingFollowUp} />
            <Stack.Screen name="Customers" component={Customers} />
            <Stack.Screen name="Notification" component={Notification} />
            <Stack.Screen name="EmployeeDashboard" component={EmployeeDashboard} />
        </Stack.Navigator>
    )
}

export default class Route extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            id: '',
            unique_id: '',
            fname: '',
            lname: '',
            profile_img: '',
            mobile: '',
            emailid: '',
            country_id: '',
            country: '',
            state_id: '',
            city_id: '',
            state: '',
            city: '',
            urole: '',
            urole_name: '',
        };
        this.checkAuthStatus();
        Global.main = this;
    }

    setting() {

    }

    onReceived(data) {
        console.log('onReceived: ', data)
    }
    onOpened(data) {
        console.log('onOpened: ', data)
    }

    async checkAuthStatus() {
        // AsyncStorage.clear()
        const id = await AsyncStorage.getItem('id');
        const unique_id = await AsyncStorage.getItem('unique_id');
        const fname = await AsyncStorage.getItem('fname');
        const lname = await AsyncStorage.getItem('lname');
        const profile_img = await AsyncStorage.getItem('profile_img');
        const mobile = await AsyncStorage.getItem('mobile');
        const emailid = await AsyncStorage.getItem('emailid');
        const country_id = await AsyncStorage.getItem('country_id');
        const country = await AsyncStorage.getItem('country');
        const state_id = await AsyncStorage.getItem('state_id');
        const city_id = await AsyncStorage.getItem('city_id');
        const state = await AsyncStorage.getItem('state');
        const city = await AsyncStorage.getItem('city');
        const urole = await AsyncStorage.getItem('urole');
        const urole_name = await AsyncStorage.getItem('urole_name');
        // alert(id)

        OneSignal.setLogLevel(6, 0);
        OneSignal.setAppId("faed007e-69b4-4122-80ca-962ae21d4dcc");

        // OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
        //     console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
        // });

        // OneSignal.setNotificationOpenedHandler(notification => {
        //     console.log("OneSignal: notification opened:", notification);
        // });

        OneSignal.promptForPushNotificationsWithUserResponse(response => {
            console.log("Prompt response:", response);
        });

        const deviceState = await OneSignal.getDeviceState();
        // console.log(deviceState)

        await setTimeout(() => {
            SplashScreen.hide();
            this.setState({
                isLoading: false,
                id: id,
                unique_id: unique_id,
                fname: fname,
                lname: lname,
                profile_img: profile_img,
                mobile: mobile,
                emailid: emailid,
                country_id: country_id,
                country: country,
                state_id: state_id,
                city_id: city_id,
                state: state,
                city: city,
                urole: urole,
                urole_name: urole_name,
                deviceToken: deviceState.userId,
            })
        }, 10);
    }

    render() {
        const { isLoading, id } = this.state;
        if (isLoading) {
            return (
                <View style={{ justifyContent: 'center', flex: 1 }}>
                    <ActivityIndicator />
                </View>
            )
        }
        return (
            <NavigationContainer>
                {!id ?
                    <Stack.Navigator initialRouteName="Login" headerMode="none">
                        <Stack.Screen name="Login" component={Login} />
                        <Stack.Screen name="Register" component={Register} />
                    </Stack.Navigator>
                    :
                    <Drawer.Navigator
                        screenOptions={{
                            swipeEnabled: true,
                            gestureEnabled: true
                        }}
                        drawerStyle={{
                            width: '75%'
                        }}
                        drawerType="slide"
                        drawerContent={(props) => <CustomDrawerMenu {...props} />}
                    >
                        <Drawer.Screen name="Root" component={StateNavigation} />
                        {/* <Drawer.Screen name="Root" component={Home} /> */}
                    </Drawer.Navigator>
                }
            </NavigationContainer>
        );
    }
}
