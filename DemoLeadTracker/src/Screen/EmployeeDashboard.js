import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, ActivityIndicator, RefreshControl, FlatList } from 'react-native';
import moment from 'moment';
import { color, font, shadow } from '../Component/Styles';
import DatePicker from '../Component/DatePicker';
import SnackBar from '../Component/SnackBar';
import Global from '../Global';
import { API } from '../Privet';
import Picker from '../Component/Picker';

var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();

export default class EmployeeDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            from_date: new Date(moment('01-04-' + year, 'DD-MM-YYYY')),
            to_date: new Date(),
            data: [],
            dataList: [],
            assignList: [],
            assign_to: [],
            active: 0,
        };
    }

    componentDidMount() {
        this.assignList();
        const { id, urole, fname, lname } = Global.main.state;
        if (urole != '1') {
            this.setState({ assign_to: { employee_id: id, employee_name: fname + ' ' + lname } }, () => this.dashboard())
        } else {
            this.dashboard();
        }
    }

    // assignList
    assignList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_assign_to, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ assignList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    dashboard() {
        const { from_date, to_date, assign_to } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isLoading: true });

        const data = {
            user_id: id,
            utype: urole,
            from_date: moment(from_date).format('DD-MM-YYYY'),
            end_date: moment(to_date).format('DD-MM-YYYY'),
            search_assign_to: assign_to.employee_id,
        }

        // console.log(data);

        fetch(API.employee_dashboard, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isLoading: false })
                if (res.status) {
                    this.setState({ data: res.data, dataList: res.data.dataemp })
                }
                else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    totalLead() {
        const { data } = this.state;
        if (data.lead_status) {
            console.log(data.lead_status.total_order_received_amount, data.lead_status.total_0_approx_amount, data.lead_status.total_1_approx_amount, data.lead_status.total_2_approx_amount, data.lead_status.total_order_loss_amount)
            const total = parseInt(data.lead_status.total_order_received_amount.replace(',', '')) + parseInt(data.lead_status.total_0_approx_amount.replace(',', '')) + parseInt(data.lead_status.total_1_approx_amount.replace(',', '')) + parseInt(data.lead_status.total_2_approx_amount.replace(',', '')) - parseInt(data.lead_status.total_order_loss_amount.replace(',', ''))
            return total;
        }
    }

    render() {
        const { isLoading, from_date, to_date, data, dataList, assign_to, assignList, active } = this.state;
        const { id, urole, fname, lname } = Global.main.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <SafeAreaView style={{ backgroundColor: color.primeColor }} />
                <StatusBar backgroundColor={color.primeColor} barStyle="light-content" />
                {/* <View style={{ backgroundColor: color.primeColor, height: 180, width: '100%', position: 'absolute' }} /> */}
                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: color.primeColor, }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 22, width: 22, resizeMode: 'contain', tintColor: '#fff' }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 20, paddingHorizontal: 5, flex: 1, color: '#fff' }}>Employee Dashboard</Text>
                </View>

                <View style={{ paddingVertical: '3%', justifyContent: 'center' }}>
                    <View style={{ top: 0, position: 'absolute', backgroundColor: color.primeColor, height: '62%', width: '100%' }} />
                    <View style={{ ...shadow, marginHorizontal: 15, paddingVertical: 8, borderRadius: 7 }}>
                        <View style={{ flexDirection: 'row', paddingTop: 8, borderRadius: 7, marginBottom: 5 }}>
                            <DatePicker
                                mainViewStyle={{ flex: 1, }}
                                placeholder="From Date"
                                maxDate={to_date}
                                value={from_date}
                                onChange={from_date => this.setState({ from_date }, () => this.dashboard())} />

                            <DatePicker
                                mainViewStyle={{ flex: 1, marginLeft: 0 }}
                                placeholder="To Date"
                                minDate={from_date}
                                maxDate={new Date()}
                                value={to_date}
                                onChange={to_date => this.setState({ to_date }, () => this.dashboard())} />
                        </View>

                        {urole === '1' ? <Picker
                            placeholder="Assign To"
                            mainViewStyle={{ marginBottom: 8, marginTop: 16 }}
                            data={assignList}
                            searchBar
                            nameKey="employee_name"
                            value={assign_to.employee_name}
                            onSelect={assign_to => this.setState({ assign_to }, () => this.dashboard())}
                        /> : null}
                    </View>
                </View>

                <ScrollView style={{ flexGrow: 1, paddingTop: 10 }}
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.setState({ from_date: new Date(moment('01-04-' + year, 'DD-MM-YYYY')), to_date: new Date(), isLoading: true, assign_to: urole === '1' ? [] : { employee_id: id, employee_name: fname + ' ' + lname } }, () => this.dashboard())} />} >

                    {dataList.length != 0 ? <View style={{ ...shadow, marginBottom: 5, marginHorizontal: 15, borderRadius: 7 }}>
                        <TouchableOpacity onPress={() => this.setState({ active: !active })} style={{ flexDirection: 'row', paddingVertical: 8, paddingHorizontal: 15, borderTopLeftRadius: 6, borderTopRightRadius: 6, backgroundColor: color.primeColor }}>
                            <Text style={{ flex: 1, color: '#fff', fontSize: 15, fontFamily: font.bold }}>Total Leads</Text>
                            <Image source={require('../assets/img/back.png')} style={{ height: 17, width: 17, resizeMode: 'contain', tintColor: '#fff', transform: [{ rotate: active ? '-90deg' : '90deg' }] }} />
                        </TouchableOpacity>

                        {active ? <View style={{ paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Total Lead</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_lead_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_lead_amount_emp}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Hot Leads</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_hot_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_hot_amount_emp}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Warm Leads</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_warm_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_warm_amount_emp}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Cold Leads</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_cold_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_cold_amount_emp}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Order Recieved</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_order_received_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_order_received_amount_emp}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 12 }}>
                                <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Order Loss</Text>
                                <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{data.total_order_loss_emp}</Text>
                                <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {data.total_order_loss_amount_emp}</Text>
                            </View>
                        </View> : null}
                    </View> : null}

                    <FlatList
                        contentContainerStyle={{ flexGrow: 1, paddingVertical: 15, paddingHorizontal: 15 }}
                        data={dataList}
                        renderItem={({ item, index }) =>
                            <View style={{ ...shadow, marginBottom: 20, borderRadius: 7 }}>
                                <View style={{ paddingVertical: 8, paddingHorizontal: 15, borderTopLeftRadius: 6, borderTopRightRadius: 6, backgroundColor: color.primeColor }}>
                                    <Text style={{ color: '#fff', fontSize: 15, fontFamily: font.bold }}>{item.name}</Text>
                                </View>

                                <View style={{ paddingHorizontal: 15 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Total Lead</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_lead}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_lead_amount}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Hot Leads</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_hot}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_hot_amount}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Warm Leads</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_warm}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_warm_amount}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Cold Leads</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_cold}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_cold_amount}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Order Recieved</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_order_received}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_order_received_amount}</Text>
                                    </View>

                                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 12 }}>
                                        <Text style={{ fontFamily: font.reg, width: '56%', marginRight: '2%' }}>Order Loss</Text>
                                        <Text style={{ fontFamily: font.reg, width: '10%', marginRight: '2%' }}>{item.total_order_loss}</Text>
                                        <Text style={{ fontFamily: font.reg, width: '30%', }}>Rs. {item.total_order_loss_amount}</Text>
                                    </View>
                                </View>
                            </View>
                        }
                    />
                </ScrollView>
                <SnackBar ref="SnackBar" />
            </View>
        );
    }
}
