import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, ActivityIndicator, RefreshControl } from 'react-native';
import moment from 'moment';
import OneSignal from 'react-native-onesignal';
import QueryString from 'query-string';
import { color, font, shadow } from '../Component/Styles';
import DatePicker from '../Component/DatePicker';
import SnackBar from '../Component/SnackBar';
import Global from '../Global';
import { API } from '../Privet';
import AppUpdateAlert from '../Component/AppUpdateAlert';

var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();

export default class Home extends Component {
    constructor(props) {
        super(props);

        // console.log(moment('1-' + month + '-' + year, 'D-M-YYYY').format('DD-MM-YYYY'));

        this.state = {
            isLoading: true,
            from_date: new Date(moment('1-' + month + '-' + year, 'D-M-YYYY')),
            to_date: new Date(),
            data: []
        };
    }

    componentDidMount() {
        this.dashboard();

        OneSignal.setLogLevel(6, 0);

        OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
            console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
        });

        OneSignal.setNotificationOpenedHandler(async data => {
            console.log("OneSignal: notification opened:", data.notification);
            if (data.notification.additionalData) {
                if (await data.notification.additionalData.link) {
                    let params = await QueryString.parseUrl(await data.notification.additionalData.link);
                    console.log(params);
                    if (params.query.pid === "view_lead_detail") {
                        await this.props.navigation.navigate('LeadDetails', { lead_id: params.query.id })
                        // that.props.navigation.navigate('SuperAdminInquiryDetail', { inquiry_id: params.query.id })
                    }
                }
            }
        });
    }

    dashboard() {
        const { from_date, to_date } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isLoading: true });

        const data = {
            user_id: id,
            utype: urole,
            from_date: moment(from_date).format('DD-MM-YYYY'),
            end_date: moment(to_date).format('DD-MM-YYYY')
        }

        // console.log(data);

        fetch(API.dashboard, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isLoading: false })
                if (res.status) {
                    this.setState({ data: res.data })
                }
                else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    render() {
        const { isLoading, from_date, to_date, data } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <SafeAreaView style={{ backgroundColor: color.primeColor }} />
                <StatusBar backgroundColor={color.primeColor} barStyle="light-content" />
                {/* <View style={{ backgroundColor: color.primeColor, height: 180, width: '100%', position: 'absolute' }} /> */}
                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: color.primeColor, }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 22, width: 22, resizeMode: 'contain', tintColor: '#fff' }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 20, paddingHorizontal: 5, flex: 1, color: '#fff' }}>Home</Text>
                </View>

                <View style={{ paddingVertical: '3%', justifyContent: 'center' }}>
                    <View style={{ top: 0, position: 'absolute', backgroundColor: color.primeColor, height: '70%', width: '100%' }} />
                    <View style={{ ...shadow, flexDirection: 'row', marginHorizontal: 15, paddingVertical: 8, borderRadius: 7 }}>
                        <DatePicker
                            mainViewStyle={{ flex: 1, marginBottom: 8, }}
                            placeholder="From Date"
                            maxDate={to_date}
                            value={from_date}
                            onChange={from_date => this.setState({ from_date }, () => this.dashboard())} />

                        <DatePicker
                            mainViewStyle={{ flex: 1, marginBottom: 8, marginLeft: 0 }}
                            placeholder="To Date"
                            minDate={from_date}
                            maxDate={new Date()}
                            value={to_date}
                            onChange={to_date => this.setState({ to_date }, () => this.dashboard())} />
                    </View>
                </View>

                <ScrollView style={{ flexGrow: 1, }}
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.setState({ from_date: new Date(moment('1-' + month + '-' + year, 'D-M-YYYY')), to_date: new Date(), isLoading: true }, () => this.dashboard())} />} >

                    <View style={{ flex: 1, ...shadow, borderRadius: 5, marginTop: 15, marginHorizontal: 15, marginBottom: 20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead')} style={{ padding: 15, borderBottomWidth: 1, }}>
                            {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.primeColor }}>{data.total_lead ? data.total_lead : '0'}</Text>}
                            <Text style={{ fontFamily: font.reg, fontSize: 16, }}>Total Lead</Text>
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '2', name: 'Hot' } })} style={{ flex: 1, alignItems: 'center', padding: 15 }}>
                                {isLoading ? <ActivityIndicator color={color.primeColor} style={{ marginBottom: 8, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 8, color: color.hot }}>{data.lead_status ? data.lead_status.total_2 : '0'}</Text>}
                                <Text style={{ fontFamily: font.reg, fontSize: 13, textAlign: 'center' }}>Hot Leads</Text>
                            </TouchableOpacity>
                            <View style={{ height: '50%', width: 1, backgroundColor: color.borderColor }} />
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '1', name: 'Warm' } })} style={{ flex: 1, alignItems: 'center', padding: 15 }}>
                                {isLoading ? <ActivityIndicator color={color.primeColor} style={{ marginBottom: 8, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 8, color: color.warm }}>{data.lead_status ? data.lead_status.total_1 : '0'}</Text>}
                                <Text style={{ fontFamily: font.reg, fontSize: 13, textAlign: 'center' }}>Warm Leads</Text>
                            </TouchableOpacity>
                            <View style={{ height: '50%', width: 1, backgroundColor: color.borderColor }} />
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '0', name: 'Cold' } })} style={{ flex: 1, alignItems: 'center', padding: 15 }}>
                                {isLoading ? <ActivityIndicator color={color.primeColor} style={{ marginBottom: 8, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 8, color: color.cold }}>{data.lead_status ? data.lead_status.total_0 : '0'}</Text>}
                                <Text style={{ fontFamily: font.reg, fontSize: 13, textAlign: 'center' }}>Cold Leads</Text>
                            </TouchableOpacity>
                        </View>

                        <Image source={require('../assets/img/total_leads.png')} style={{ position: 'absolute', right: 0, top: 0, height: 50, width: 50, borderTopRightRadius: 7, tintColor: color.primeColor }} />
                    </View>

                    <View style={{ flexDirection: 'row', paddingHorizontal: 15, marginBottom: 20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '4', name: 'Order Loss' } })} style={{ flex: 1, ...shadow, borderRadius: 5, marginRight: 20, padding: 15 }}>
                            {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.red }}>{data.lead_status ? data.lead_status.total_4 : '0'}</Text>}
                            <Text style={{ fontFamily: font.reg, }}>Order Loss</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '3', name: 'Order Received' } })} style={{ flex: 1, ...shadow, borderRadius: 5, padding: 15 }}>
                            {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.green }}>{data.lead_status ? data.lead_status.total_3 : '0'}</Text>}
                            <Text style={{ fontFamily: font.reg, }}>Order Received</Text>
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '4', name: 'Order Loss' } })} style={{ ...shadow, borderRadius: 5, marginHorizontal: 15, marginBottom: 20, padding: 15 }}>
                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.red }}>Rs. {data.lead_status ? data.lead_status.total_order_loss_amount : '0'}</Text>}
                        <Text style={{ fontFamily: font.reg, fontSize: 16 }}>Order Loss</Text>
                        <Image source={require('../assets/img/wallet_amount_icon.png')} style={{ position: 'absolute', right: 0, height: 35, width: 35, borderTopRightRadius: 5, tintColor: color.red }} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '3', name: 'Order Received' } })} style={{ ...shadow, borderRadius: 5, marginHorizontal: 15, marginBottom: 20, padding: 15 }}>
                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.green }}>Rs. {data.lead_status ? data.lead_status.total_order_received_amount : '0'}</Text>}
                        <Text style={{ fontFamily: font.reg, fontSize: 16 }}>Order Received</Text>
                        <Image source={require('../assets/img/wallet_amount_icon.png')} style={{ position: 'absolute', right: 0, height: 35, width: 35, borderTopRightRadius: 5, tintColor: color.green }} />
                    </TouchableOpacity>
                </ScrollView>
                <SnackBar ref="SnackBar" />
                <AppUpdateAlert />
            </View>
        );
    }
}
