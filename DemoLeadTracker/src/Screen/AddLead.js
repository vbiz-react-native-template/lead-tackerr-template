import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, KeyboardAvoidingView, Platform, Keyboard, ActivityIndicator } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import SnackBar from '../Component/SnackBar';
import Picker from '../Component/Picker';
import DatePicker from '../Component/DatePicker';
import { API } from '../Privet';
import Global from '../Global';
import moment from 'moment';

export default class AddLead extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            lead_id: '',
            c_name: '',
            o_name: '',
            o_mobile: '',
            o_email: '',
            website: '',
            address: '',
            country: [],
            state: [],
            city: [],

            contact_person_details: [{ cperson: '', cmobile: '', cemail: '' }],

            lead_date: new Date(),
            generated_by: { employee_id: Global.main.state.id, employee_name: Global.main.state.fname + ' ' + Global.main.state.lname },
            assign_to: [],
            monitor_by: [],
            lead_source: [],
            products_services: [],
            approx_amount: '',
            lead_status: { id: '0', name: 'Cold' },

            next_followup_date: null,
            next_followup_time: null,
            followup_remarks: '',

            personal_meeting_required: '1',
            meeting_date: new Date(),
            meeting_from_time: null,
            meeting_to_time: null,
            meeting_remarks: '',

            quotation_required: '1',
            quotation_date: new Date(),
            quotation_attachment: [],
            quotation_remarks: '',

            genetratedList: [],
            assignList: [],
            monitorList: [],
            products_servicesList: [],
            countryList: [],
            stateList: [],
            cityList: [],
            leadStatusList: [{ id: '0', name: 'Cold' }, { id: '1', name: 'Warm' }, { id: '2', name: 'Hot' }, { id: '3', name: 'Order Received' }, { id: '4', name: 'Order Loss' }],
            leadSourceList: [],

            company_suggationLoading: false,
            company_suggation: [],
            onFocusCompany: false,

            srno: '',
        };
    }

    componentDidMount() {
        const { route } = this.props;
        if (route.params && route.params.item.inquiry_id) {
            // console.log(route.params.item);
            let contact_person_details = [];
            for (let i = 0; i < route.params.item.lead_contacts.length; i++) {
                contact_person_details.push({ cperson: route.params.item.lead_contacts[i].cperson_name, cmobile: route.params.item.lead_contacts[i].cperson_mobile, cemail: route.params.item.lead_contacts[i].cperson_email })
            }

            let product_id = route.params.item.product_id.split(',');
            let product_name = route.params.item.product_name.split(',');
            let products_services = [];
            for (let i = 0; i < product_id.length; i++) {
                products_services.push({ id: product_id[i], name: product_name[i], isSelected: true });
            }
            // console.log(product_name)
            const approx_amount = route.params.item.approx_amount
            // alert(approx_amount.replace(/,/g, ''))
            this.setState({
                lead_id: route.params.item.inquiry_id,
                c_name: route.params.item.company,
                o_name: route.params.item.owner_name,
                o_mobile: route.params.item.owner_mobile,
                o_email: route.params.item.owner_email,
                website: route.params.item.website,
                address: route.params.item.address,
                country: { id: route.params.item.country_id, name: route.params.item.country_name },
                state: { id: route.params.item.state_id, sname: route.params.item.state_name },
                city: { id: route.params.item.city_id, cname: route.params.item.city_name },

                contact_person_details,

                lead_date: new Date(moment(route.params.item.idate, 'DD-MM-YYYY')),
                generated_by: { employee_id: route.params.item.gen_by, employee_name: route.params.item.gen_by_name },
                assign_to: { employee_id: route.params.item.assign_to, employee_name: route.params.item.assign_to_name },
                monitor_by: { employee_id: route.params.item.monitor_by, employee_name: route.params.item.monitor_by_name },
                lead_source: { id: route.params.item.inq_source, name: route.params.item.inq_source_name },
                products_services,
                approx_amount: approx_amount.replace(/,/g, ''),
                lead_status: { id: route.params.item.status, name: route.params.item.status_name },
                srno: route.params.item.srno,
            })
            this.stateList({ id: route.params.item.country_id });
            this.cityList({ id: route.params.item.state_id });
        } else {
            this.srno();
        }

        this.genetratedList();
        this.monitorList();
        this.assignList();
        this.countryList();
        this.products_servicesList();
        this.leadSourceList();
    }

    // srno API
    srno() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.lead_srno, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                console.log(res);
                if (res.status) {
                    this.setState({ srno: res.data[0].srno, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // company_suggation API
    company_suggation(value) {
        this.setState({ c_name: value });
        if (value.length >= 3) {
            this.setState({ company_suggationLoading: true });
            const { id, urole } = Global.main.state;
            const data = {
                user_id: id,
                utype: urole,
                search_company: value
            }

            fetch(API.get_company_list, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    console.log(res)
                    if (res.status) {
                        this.setState({ company_suggationLoading: false, company_suggation: res.data, })
                    } else {
                        this.setState({ company_suggationLoading: false, company_suggation: [] })
                    }
                })
                .catch((error) => {
                    this.setState({ company_suggationLoading: false, })
                    console.log('error: ', error);
                });

        } else {
            this.setState({ company_suggation: [] })
        }
    }

    companyDetails(item) {
        Keyboard.dismiss();
        this.setState({
            company_suggation: [],
            onFocusCompany: false,
            c_name: item.company,
            o_name: item.owner_name,
            o_mobile: item.owner_mobile,
            o_email: item.owner_email,
            website: item.website,
            address: item.address,
            country: { id: item.country_id, name: item.country },
            state: { id: item.state_id, sname: item.state },
            city: { id: item.city_id, cname: item.city },
            contact_person_details: item.contacts
        })
        this.stateList({ id: item.country_id });
        this.cityList({ id: item.state_id });
    }

    // genetratedList
    genetratedList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_generated_by, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ genetratedList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // monitorList
    monitorList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_monitor_by, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ monitorList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }
    // assignList
    assignList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_assign_to, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ assignList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // countryList
    countryList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.country_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            // body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ countryList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // stateList
    stateList(country_id) {
        const { id, urole } = Global.main.state;
        const data = {
            // user_id: id,
            // utype: urole,
            country_id: country_id.id
        }

        // console.log(data)

        fetch(API.state_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('stateList: ', res)
                if (res.status) {
                    this.setState({ stateList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // cityList
    cityList(state_id) {
        const { id, urole } = Global.main.state;
        const data = {
            // user_id: id,
            // utype: urole,
            state_id: state_id.id
        }

        fetch(API.city_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('ciryList: ', res)
                if (res.status) {
                    this.setState({ cityList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // products_servicesList
    products_servicesList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_products_services, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ products_servicesList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // leadSourceList
    leadSourceList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_lead_source, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res)
                if (res.status) {
                    this.setState({ leadSourceList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    async selectFile() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            this.setState({ quotation_attachment: res })
            // console.log(res);
        } catch (err) {
            console.log('selectFile error: ', err)
        }
    }

    addCPD() {
        const { contact_person_details } = this.state;
        this.setState({ contact_person_details: [...contact_person_details, { cperson: '', cmobile: '', cemail: '' }] })
    }

    removeCPD() {
        const { contact_person_details } = this.state;
        contact_person_details.pop();
        this.setState({ contact_person_details })
    }

    add() {
        const {
            isLoading, lead_id, c_name, o_name, o_mobile, o_email, website, address, country, state, city, contact_person_details, lead_date, generated_by, assign_to, monitor_by, lead_source, products_services, approx_amount, lead_status,
            next_followup_date, next_followup_time, followup_remarks,
            personal_meeting_required, meeting_date, meeting_from_time, meeting_to_time, meeting_remarks,
            quotation_required, quotation_date, quotation_attachment, quotation_remarks, quotation_amount,
            products_servicesList, employeeList, stateList, countryList, leadStatusList, cityList, leadSourceList,
        } = this.state;
        const { id, urole } = Global.main.state;
        Keyboard.dismiss();

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        let contact_person_detailsError = "";
        for (let i = 0; i < contact_person_details.length; i++) {
            if (contact_person_details[i].cperson === '' || contact_person_details[i].cmobile === '' || contact_person_details[i].cmobile.length != 10) {
                contact_person_detailsError = "Please enter contact person name and mobile."
            }
        }

        let products_servicesID = "";
        for (let i = 0; i < products_services.length; i++) {
            products_servicesID = products_servicesID + ',' + products_services[i].id;
        }
        products_servicesID = products_servicesID.substring(1);
        // console.log(products_servicesID);

        if (!c_name) {
            this.refs.SnackBar.show('Please enter company name.');
        }
        else if (!o_name) {
            this.refs.SnackBar.show('Please enter company owner name.');
        }
        else if (!o_mobile) {
            this.refs.SnackBar.show('Please enter company owner mobile no.');
        }
        else if (o_mobile.length != 10) {
            this.refs.SnackBar.show('Please enter company owner velide mobile no.');
        }
        // else if (!o_email) {
        //     this.refs.SnackBar.show('Please enter company owner email address.');
        // }
        else if (o_email && !reg.test(o_email)) {
            this.refs.SnackBar.show("Please enter company owner valid email address.");
        }
        // else if (!website) {
        //     this.refs.SnackBar.show('Please enter company website.');
        // }
        else if (!address) {
            this.refs.SnackBar.show('Please enter address.');
        }
        else if (!country.id) {
            this.refs.SnackBar.show('Please select country.');
        }
        else if (!state.id) {
            this.refs.SnackBar.show('Please select state.');
        }
        else if (!city.id) {
            this.refs.SnackBar.show('Please select city.');
        }
        else if (contact_person_detailsError) {
            this.refs.SnackBar.show(contact_person_detailsError);
        }
        else if (!assign_to.employee_id) {
            this.refs.SnackBar.show('Please select assign to.');
        }
        else if (!monitor_by.employee_id) {
            this.refs.SnackBar.show('Please select monitor by.');
        }
        else if (!lead_source.id) {
            this.refs.SnackBar.show('Please select lead source.');
        }
        else if (!products_servicesID) {
            this.refs.SnackBar.show('Please select products/services.');
        }
        // else if (!approx_amount) {
        //     this.refs.SnackBar.show('Please enter approx amount.');
        // }

        else {
            this.setState({ isLoading: true });
            let formdata = new FormData();

            formdata.append('utype', urole);
            formdata.append('user_id', id);
            formdata.append('idate', moment(lead_date).format('DD-MM-YYYY'));
            formdata.append('company', c_name);
            formdata.append('owner_name', o_name);
            formdata.append('owner_mobile', o_mobile);
            formdata.append('owner_email', o_email);
            formdata.append('contact_person', JSON.stringify(contact_person_details));
            formdata.append('address', address);
            formdata.append('caddress', '');
            formdata.append('add_lat', '0');
            formdata.append('add_log', '0');
            formdata.append('country_id', country.id);
            formdata.append('state_id', state.id);
            formdata.append('city_id', city.id);
            formdata.append('gen_by', generated_by.employee_id);
            formdata.append('assign_to', assign_to.employee_id);
            formdata.append('monitor_by', monitor_by.employee_id);
            formdata.append('lead_source', lead_source.id);
            formdata.append('product_id', products_servicesID);
            formdata.append('approx_amount', approx_amount);
            formdata.append('lead_status', lead_status.id);
            formdata.append('website', website);
            formdata.append('lead_id', lead_id);

            formdata.append('nremarks', followup_remarks);
            formdata.append('nfdate', moment(next_followup_date).format('DD-MM-YYYY'));
            formdata.append('nftime', moment(next_followup_time).format('LT'));

            formdata.append('meeting_required', personal_meeting_required === '1' ? 'yes' : 'no');
            formdata.append('meeting_date', moment(meeting_date).format('DD-MM-YYYY'));
            formdata.append('from_time', moment(meeting_from_time).format('LT'));
            formdata.append('to_time', moment(meeting_to_time).format('LT'));
            formdata.append('meeting_remarks', meeting_remarks);

            formdata.append('quo_required', quotation_required === '1' ? 'yes' : 'no');
            formdata.append('qdate', moment(quotation_date).format('DD-MM-YYYY'));
            formdata.append('qamount', quotation_amount);
            formdata.append('qremarks', quotation_remarks);
            formdata.append('attachment', quotation_attachment.uri ? { uri: quotation_attachment.uri, name: quotation_attachment.name, type: quotation_attachment.type } : '');

            fetch(lead_id ? API.edit_lead : API.add_lead, {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data',
                },
                body: formdata
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    console.log(res);
                    this.setState({ isLoading: false });
                    if (res.status) {
                        this.props.navigation.goBack();
                    } else {
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    render() {
        const {
            isLoading, lead_id, c_name, o_name, o_mobile, o_email, website, address, country, state, city, contact_person_details, lead_date, generated_by, assign_to, monitor_by, lead_source, products_services, approx_amount, lead_status,
            next_followup_date, next_followup_time, followup_remarks,
            personal_meeting_required, meeting_date, meeting_from_time, meeting_to_time, meeting_remarks,
            quotation_required, quotation_date, quotation_attachment, quotation_remarks, quotation_amount,
            products_servicesList, genetratedList, assignList, monitorList, stateList, countryList, leadStatusList, cityList, leadSourceList,
            company_suggationLoading, onFocusCompany, company_suggation, srno
        } = this.state;

        // console.log(lead_id)

        return (
            <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#F3F3F3' }} behavior={Platform.OS === 'ios' ? 'padding' : null} >
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/back.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>{lead_id ? 'Edit Lead' : 'Add Lead'}</Text>
                </View>

                <ScrollView contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingVertical: 15 }} >

                    {/* Company Details */}
                    <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Lead No.: {srno}</Text>
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                autoCapitalize="words"
                                label="Company Name"
                                value={c_name}
                                onChangeText={c_name => this.company_suggation(c_name)}
                                onFocus={() => this.setState({ onFocusCompany: true })}
                                onBlur={() => this.setState({ onFocusCompany: false, company_suggation: [] })}
                            />
                        </View>

                        {company_suggationLoading && <ActivityIndicator style={{ marginBottom: 8 }} color={color.primeColor} />}
                        {onFocusCompany && company_suggation.length != 0 && c_name.length >= 3 ?
                            <View style={{ maxHeight: 200, marginHorizontal: 15, marginBottom: 15 }}>

                                <FlatList
                                    contentContainerStyle={{ paddingHorizontal: 0 }}
                                    data={company_suggation}
                                    renderItem={({ item, index }) =>
                                        <TouchableOpacity key={index + ''} onPress={() => this.companyDetails(item)} style={{ paddingVertical: 10, borderBottomWidth: 1 }}>
                                            <Text style={{ fontFamily: font.reg }}>{item.company}</Text>
                                        </TouchableOpacity>
                                    }
                                />
                            </View>
                            : null
                        }

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                autoCapitalize="words"
                                label="Owner Name"
                                value={o_name}
                                onChangeText={o_name => this.setState({ o_name })}
                            />
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                keyboardType="number-pad"
                                label="Owner Mobile"
                                maxLength={10}
                                value={o_mobile}
                                onChangeText={o_mobile => this.setState({ o_mobile })}
                            />
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                autoCapitalize="none"
                                keyboardType="email-address"
                                label="Owner Email Id"
                                value={o_email}
                                onChangeText={o_email => this.setState({ o_email })}
                            />
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                autoCapitalize="none"
                                keyboardType="url"
                                label="Website"
                                value={website}
                                onChangeText={website => this.setState({ website })}
                            />
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                autoCapitalize="sentences"
                                multiline={true}
                                style={{ height: 90, textAlignVertical: 'top' }}
                                blurOnSubmit={false}
                                label="Address"
                                value={address}
                                onChangeText={address => this.setState({ address })}
                            />
                        </View>

                        <View style={{ marginBottom: 15 }}>
                            <Picker
                                placeholder="Country"
                                data={countryList}
                                searchBar
                                value={country.name}
                                onSelect={_country => _country.id != country.id && this.setState({ country: _country, status: [], city: [], stateList: [], cityList: [], }, () => this.stateList(_country))}
                            />
                        </View>

                        <View style={{ marginBottom: 20, flexDirection: 'row' }}>
                            <Picker
                                placeholder="State"
                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                data={stateList}
                                searchBar
                                nameKey="sname"
                                value={state.sname}
                                onSelect={_state => _state.id != state.id && this.setState({ state: _state, city: [], cityList: [], }, () => this.cityList(_state))}
                            />
                            <Picker
                                placeholder="City"
                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                data={cityList}
                                searchBar
                                nameKey="cname"
                                value={city.cname}
                                onSelect={city => this.setState({ city })}
                            />
                        </View>

                    </View>

                    {/* Contact Person Details */}
                    <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Contact Person Details</Text>
                        </View>

                        <FlatList
                            data={contact_person_details}
                            renderItem={({ item, index }) =>
                                <View style={{ borderBottomWidth: contact_person_details.length - 1 == index ? 0 : 1, marginBottom: contact_person_details.length - 1 == index ? 0 : 15, paddingBottom: 8 }}>
                                    <View style={{ marginBottom: 15 }}>
                                        <FloatingLabelInput
                                            autoCapitalize="words"
                                            label="Person Name"
                                            value={item.cperson}
                                            onChangeText={cperson => {
                                                contact_person_details[index].cperson = cperson
                                                this.setState({ contact_person_details })
                                            }}
                                        />
                                    </View>

                                    <View style={{ marginBottom: 15 }}>
                                        <FloatingLabelInput
                                            label="Person Mobile"
                                            keyboardType="number-pad"
                                            maxLength={10}
                                            value={item.cmobile}
                                            onChangeText={cmobile => {
                                                contact_person_details[index].cmobile = cmobile
                                                this.setState({ contact_person_details })
                                            }}
                                        />
                                    </View>

                                    <View style={{ marginBottom: 15 }}>
                                        <FloatingLabelInput
                                            autoCapitalize="none"
                                            label="Person Email Id"
                                            keyboardType="email-address"
                                            value={item.cemail}
                                            onChangeText={cemail => {
                                                contact_person_details[index].cemail = cemail
                                                this.setState({ contact_person_details })
                                            }}
                                        />
                                    </View>
                                </View>
                            }
                        />

                        <View style={{ marginBottom: 20, marginHorizontal: 15, flexDirection: 'row' }}>
                            <View borderColor={color.primeColor} style={{ flex: 1, borderWidth: 1, borderRadius: 8 }}>
                                <TouchableOpacity onPress={() => this.addCPD()} style={{ height: 35, justifyContent: 'center', alignItems: 'center', }}>
                                    <Image source={require('../assets/img/cancle_icon.png')} style={{ height: 15, width: 15, resizeMode: 'contain', tintColor: color.primeColor, transform: [{ rotate: '45deg' }] }} />
                                </TouchableOpacity>
                            </View>

                            {contact_person_details.length > 1 && <View borderColor={color.red} style={{ marginLeft: 15, flex: 1, borderWidth: 1, borderRadius: 8 }}>
                                <TouchableOpacity onPress={() => this.removeCPD()} style={{ height: 35, justifyContent: 'center', alignItems: 'center', }}>
                                    <Image source={require('../assets/img/cancle_icon.png')} style={{ height: 15, width: 15, resizeMode: 'contain', tintColor: color.red, }} />
                                </TouchableOpacity>
                            </View>}
                        </View>
                    </View>

                    {/* Lead Details */}
                    <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Lead Details</Text>
                        </View>

                        <DatePicker
                            mainViewStyle={{ marginBottom: 15 }}
                            placeholder="Lead Date"
                            maxDate={new Date()}
                            value={lead_date}
                            onChange={lead_date => this.setState({ lead_date })} />

                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                            <Picker
                                placeholder="Generated By"
                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                data={genetratedList}
                                searchBar
                                nameKey="employee_name"
                                value={generated_by.employee_name}
                                onSelect={generated_by => this.setState({ generated_by })}
                            />
                            <Picker
                                placeholder="Assign To"
                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                data={assignList}
                                searchBar
                                nameKey="employee_name"
                                value={assign_to.employee_name}
                                onSelect={assign_to => this.setState({ assign_to })}
                            />
                        </View>

                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                            <Picker
                                placeholder="Monitor By"
                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                data={monitorList}
                                searchBar
                                nameKey="employee_name"
                                value={monitor_by.employee_name}
                                onSelect={monitor_by => this.setState({ monitor_by })}
                            />
                            <Picker
                                placeholder="Lead Source"
                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                data={leadSourceList}
                                searchBar
                                value={lead_source.name}
                                onSelect={lead_source => this.setState({ lead_source })}
                            />
                        </View>

                        <Picker
                            multiple
                            placeholder="Products/Services"
                            mainViewStyle={{ marginBottom: 15, }}
                            data={products_servicesList}
                            searchBar
                            selected={products_services}
                            onSelect={products_services => this.setState({ products_services })}
                        />

                        <View style={{ marginBottom: 15 }}>
                            <FloatingLabelInput
                                label="Approx Amount"
                                keyboardType="number-pad"
                                maxLength={7}
                                value={approx_amount}
                                onChangeText={approx_amount => this.setState({ approx_amount })}
                            />
                        </View>

                        {!lead_id ? <Picker
                            placeholder="Lead Status"
                            mainViewStyle={{ marginBottom: 15, }}
                            data={leadStatusList}
                            searchBar
                            value={lead_status.name}
                            onSelect={lead_status => this.setState({ lead_status })}
                        /> : null}

                    </View>

                    {/* Followup Details */}
                    {!lead_id ? <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Followup Details</Text>
                        </View>

                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                            <DatePicker
                                mainViewStyle={{ flex: 1, marginRight: 15 }}
                                placeholder="Next Date"
                                value={next_followup_date}
                                minDate={new Date()}
                                onChange={next_followup_date => this.setState({ next_followup_date })} />

                            <DatePicker
                                mode="time"
                                mainViewStyle={{ flex: 1, marginLeft: 0 }}
                                placeholder="Next Time"
                                value={next_followup_time}
                                onChange={next_followup_time => this.setState({ next_followup_time })} />
                        </View>

                        <View style={{ marginBottom: 20 }}>
                            <FloatingLabelInput
                                autoCapitalize="sentences"
                                multiline={true}
                                style={{ height: 90, textAlignVertical: 'top' }}
                                blurOnSubmit={false}
                                label="Remarks"
                                value={followup_remarks}
                                onChangeText={followup_remarks => this.setState({ followup_remarks })}
                            />
                        </View>

                    </View> : null}

                    {/* Meeting Details */}
                    {!lead_id ? <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Meeting Details</Text>
                        </View>

                        <View style={{ marginBottom: 15, marginHorizontal: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.lableColor }}>Personal Meeting Required</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View borderColor={personal_meeting_required === '1' ? color.primeColor : color.lableColor} style={{ flex: 1, borderWidth: 1.5, borderRadius: 6, marginRight: 15 }}>
                                    <TouchableOpacity onPress={() => this.setState({ personal_meeting_required: '1' })} style={{ flex: 1, paddingVertical: 5, backgroundColor: personal_meeting_required === '1' ? color.primeColor : '#0000', borderRadius: 4 }}>
                                        <Text style={{ fontFamily: font.bold, textAlign: 'center', color: personal_meeting_required === '1' ? '#fff' : color.lableColor }}>YES</Text>
                                    </TouchableOpacity>
                                </View>

                                <View borderColor={personal_meeting_required === '0' ? color.primeColor : color.lableColor} style={{ flex: 1, borderWidth: 1.5, borderRadius: 5 }}>
                                    <TouchableOpacity onPress={() => this.setState({ personal_meeting_required: '0' })} style={{ flex: 1, paddingVertical: 5, backgroundColor: personal_meeting_required === '0' ? color.primeColor : '#0000', borderRadius: 4 }}>
                                        <Text style={{ fontFamily: font.bold, textAlign: 'center', color: personal_meeting_required === '0' ? '#fff' : color.lableColor }}>NO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        {personal_meeting_required === '1' ? <>
                            <DatePicker
                                mainViewStyle={{ marginBottom: 15 }}
                                placeholder="Meeting Date"
                                value={meeting_date}
                                minDate={new Date()}
                                onChange={meeting_date => this.setState({ meeting_date })} />

                            <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                <DatePicker
                                    mode="time"
                                    mainViewStyle={{ flex: 1, marginRight: 15 }}
                                    placeholder="From Time"
                                    value={meeting_from_time}
                                    minDate={new Date()}
                                    onChange={meeting_from_time => this.setState({ meeting_from_time })} />

                                <DatePicker
                                    mode="time"
                                    mainViewStyle={{ flex: 1, marginLeft: 0 }}
                                    placeholder="To Time"
                                    value={meeting_to_time}
                                    onChange={meeting_to_time => this.setState({ meeting_to_time })} />
                            </View>

                            <View style={{ marginBottom: 20 }}>
                                <FloatingLabelInput
                                    autoCapitalize="sentences"
                                    multiline={true}
                                    style={{ height: 90, textAlignVertical: 'top' }}
                                    blurOnSubmit={false}
                                    label="Remarks"
                                    value={meeting_remarks}
                                    onChangeText={meeting_remarks => this.setState({ meeting_remarks })}
                                />
                            </View>
                        </> : null}
                    </View> : null}

                    {/* Quotation Details */}
                    {!lead_id ? <View style={{ ...shadow, borderRadius: 8, marginBottom: 20 }}>
                        <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, marginBottom: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Quotation Details</Text>
                        </View>

                        <View style={{ marginBottom: 15, marginHorizontal: 15 }}>
                            <Text style={{ fontFamily: font.bold, color: color.lableColor }}>Quotation Required</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View borderColor={quotation_required === '1' ? color.primeColor : color.lableColor} style={{ flex: 1, borderWidth: 1.5, borderRadius: 6, marginRight: 15 }}>
                                    <TouchableOpacity onPress={() => this.setState({ quotation_required: '1' })} style={{ flex: 1, paddingVertical: 5, backgroundColor: quotation_required === '1' ? color.primeColor : '#0000', borderRadius: 4 }}>
                                        <Text style={{ fontFamily: font.bold, textAlign: 'center', color: quotation_required === '1' ? '#fff' : color.lableColor }}>YES</Text>
                                    </TouchableOpacity>
                                </View>

                                <View borderColor={quotation_required === '0' ? color.primeColor : color.lableColor} style={{ flex: 1, borderWidth: 1.5, borderRadius: 5 }}>
                                    <TouchableOpacity onPress={() => this.setState({ quotation_required: '0' })} style={{ flex: 1, paddingVertical: 5, backgroundColor: quotation_required === '0' ? color.primeColor : '#0000', borderRadius: 4 }}>
                                        <Text style={{ fontFamily: font.bold, textAlign: 'center', color: quotation_required === '0' ? '#fff' : color.lableColor }}>NO</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        {quotation_required === '1' ? <>
                            <DatePicker
                                mainViewStyle={{ marginBottom: 15 }}
                                placeholder="Quotation Date"
                                value={quotation_date}
                                minDate={new Date()}
                                onChange={quotation_date => this.setState({ quotation_date })} />

                            <View style={{ marginBottom: 15 }}>
                                <FloatingLabelInput
                                    label="Quotation Amount"
                                    keyboardType="number-pad"
                                    maxLength={7}
                                    value={quotation_amount}
                                    onChangeText={quotation_amount => this.setState({ quotation_amount })}
                                />
                            </View>

                            <View style={{ borderRadius: 8, marginHorizontal: 15, marginBottom: 15, borderWidth: 1, borderStyle: "dashed", marginTop: 8 }}>
                                <TouchableOpacity onPress={() => this.selectFile()} style={{ height: 60 }}>
                                    {quotation_attachment.uri ?
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 8 }}>
                                            <Image source={require('../assets/img/file.png')} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                                            <Text numberOfLines={1} style={{ flex: 1, fontFamily: font.reg, marginLeft: 10 }}>{quotation_attachment.name}</Text>
                                        </View>
                                        : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={{ fontFamily: font.bold, color: color.borderColor }}>Quotation Attachment</Text>
                                        </View>}
                                </TouchableOpacity>
                            </View>

                            <View style={{ marginBottom: 20 }}>
                                <FloatingLabelInput
                                    autoCapitalize="sentences"
                                    multiline={true}
                                    style={{ height: 90, textAlignVertical: 'top' }}
                                    blurOnSubmit={false}
                                    label="Remarks"
                                    value={quotation_remarks}
                                    onChangeText={quotation_remarks => this.setState({ quotation_remarks })}
                                />
                            </View>
                        </> : null}
                    </View> : null}

                    <TouchableOpacity onPress={() => this.add()} disabled={isLoading} style={{ height: 35, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, marginBottom: 20 }}>
                        {isLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>{lead_id ? 'UPDATE' : 'ADD'}</Text>}
                    </TouchableOpacity>

                </ScrollView>

                <SnackBar ref="SnackBar" />
            </KeyboardAvoidingView>
        );
    }
}
