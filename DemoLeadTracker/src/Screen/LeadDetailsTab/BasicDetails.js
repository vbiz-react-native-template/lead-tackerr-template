import moment from 'moment';
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, FlatList, SafeAreaView, Platform, TouchableOpacity, Linking, ActivityIndicator, RefreshControl, Modal, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Share } from 'react-native';
import Contacts from 'react-native-contacts';
import { request, PERMISSIONS } from 'react-native-permissions';
import { color, font, shadow } from '../../Component/Styles';
import SnackBar from '../../Component/SnackBar';
import Picker from '../../Component/Picker';
import DatePicker from '../../Component/DatePicker';
import FloatingLabelInput from '../../Component/FloatingLabelInput';
import Global from '../../Global';
import { API } from '../../Privet';

export default class BasicDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isStatusChangeLoading: false,
            changeStatusModalVisible: false,
            leadStatus: { name: this.props.data.status_name, id: this.props.data.status },
            leadStatusList: [{ id: '0', name: 'Cold' }, { id: '1', name: 'Warm' }, { id: '2', name: 'Hot' }, { id: '3', name: 'Order Received' }, { id: '4', name: 'Order Loss' }],
            order_amount: '',
            date: new Date(),
            reason: '',
            shareVisible: false,

            shareDocumentsList: [
                { name: 'Company Brochure', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Company Visiting Card', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Services', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Terms & Conditions', url: 'https://vbizsolutions.biz/home', isSelected: false },
            ],
            shareContact: [],
        };
    }

    componentDidMount() {
        // console.log(this.props.data)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data) {
            // console.log(nextProps.data)
            this.setState({ leadStatusList: nextProps.data.status_array, leadStatus: { id: nextProps.data.status, name: nextProps.data.status_name } })
        }
    }

    checkStatus(status) {
        if (status === '0') {
            return ({ name: 'COLD', color: color.cold })
        }
        else if (status === '1') {
            return ({ name: 'WARM', color: color.warm })
        }
        else if (status === '2') {
            return ({ name: 'HOT', color: color.hot })
        }
        else if (status === '3') {
            return ({ name: 'RECEIVED', color: color.green })
        }
        else if (status === '4') {
            return ({ name: 'LOSS', color: color.red })
        }
        else {
            return ({ name: '--', color: '#0000000' })
        }
    }

    change() {
        Keyboard.dismiss();
        const { leadStatus, order_amount, reason, date } = this.state;
        const { id, urole } = Global.main.state;

        if (leadStatus.id === '3' && !order_amount) {
            this.refs.SnackBar.show('Please enter order amount.');
        }
        else if (leadStatus.id === '4' && !reason) {
            this.refs.SnackBar.show('Please enter reason.');
        }
        else {
            this.setState({ isStatusChangeLoading: true });
            const data = {
                user_id: id,
                utype: urole,
                lead_id: this.props.lead_id,
                order_amount: order_amount,
                order_date: moment(date).format('DD-MM-YYYY'),
                reason: reason,
                lead_status: leadStatus.id + '',
            }

            fetch(API.change_lead_status, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    console.log(res);
                    this.setState({ isStatusChangeLoading: false });
                    if (res.status) {
                        this.setState({
                            changeStatusModalVisible: false,
                        });
                        this.props.onRefresh();
                    } else {
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isStatusChangeLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    saveContact(item) {
        // console.log(item)
        request(Platform.OS === "ios" ? PERMISSIONS.IOS.CONTACTS : PERMISSIONS.ANDROID.WRITE_CONTACTS).then((result) => {
            // console.log(result)
            if (result === "blocked") {
                Linking.openSettings();
            } else {
                const newPerson = {
                    phoneNumbers: [{
                        label: "mobile",
                        number: item.cperson_mobile,
                    }],
                    emailAddresses: [{
                        label: "work",
                        email: item.cperson_email,
                    }],
                    // familyName: "Nietzsche",
                    // givenName: item.cperson_name,
                    displayName: item.cperson_name,
                }

                // Contacts.addContact(newPerson);
                Contacts.openContactForm(newPerson).then(contact => {
                    console.log(contact.displayName, null)
                    // if (console.displayName) {
                    //     this.refs.SnackBarMain.show('Contact number is saved.');
                    // }
                })
            }
        });
    }

    async share() {
        const { shareDocumentsList } = this.state;

        let message = "Hello, " + "\nHow did you find our service last week? We would love to serve you better.\n";

        for (const item of shareDocumentsList) {
            if (item.isSelected) {
                // message += "\n*" + item.name + ":*\n" + item.url;
                message = message + "\n*" + item.name + ":*\n" + item.url;
            }
        }

        try {
            const result = await Share.share({
                message: message
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('activityType: ', result)
                } else {
                    console.log('shared: ', result)
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismissedAction: ', result)
            }
        } catch (error) {
            console.log(error);
        }

        this.setState({
            shareVisible: false,
            shareDocumentsList: [
                { name: 'Company Brochure', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Company Visiting Card', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Services', url: 'https://vbizsolutions.biz/home', isSelected: false },
                { name: 'Terms & Conditions', url: 'https://vbizsolutions.biz/home', isSelected: false },
            ],
        });
    }

    whatsappSend(item) {

Global.main.setState({fname:"Jay Mataji"})

        let message = "Hello, " + "%0aI'm " + Global.main.state.fname + " " + Global.main.state.lname + "%0a";

        let number = item.cperson_mobile;
        let char3 = item.cperson_mobile.slice(0, 3);
        let char2 = item.cperson_mobile.slice(0, 2);
        let char1 = item.cperson_mobile.slice(0, 1);

        if (char2 === '91') {
            number = '+' + number;
        }
        else if (char1 === '+') {
            number = number;
        }
        else if (char3 !== '+91') {
            number = '+91' + number;
        }

        Linking.openURL("whatsapp://send?text=" + message + "&phone=" + number);
        // Linking.openURL("https://wa.me/" + number + "?text=" + message);
    }

    render() {
        const { changeStatusModalVisible, shareVisible, isStatusChangeLoading, leadStatus, leadStatusList, order_amount, reason, date, shareDocumentsList } = this.state;
        const { data = [], isLoading = false, } = this.props;
        if (isLoading && data.length == 0) {
            return (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <ActivityIndicator color={color.primeColor} />
                </View>
            );
        }
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1, padding: 15, paddingBottom: 55 }}
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.props.onRefresh()} />}
                >

                    {data.length == 0 ?
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontFamily: font.bold }}>Lead details not found!</Text>
                        </View>
                        : <View>
                            {/* Company Details */}
                            <View style={{ ...shadow, borderRadius: 8, marginBottom: 20, paddingBottom: 15 }}>
                                <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, }}>
                                    <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Lead No.: {data.srno}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Company Name:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.company}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Owner Name:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.owner_name}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Owner Mobile No.:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.owner_mobile}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Owner Email Id:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.owner_email ? data.owner_email : '--'}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Address:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.address}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Country:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.country_name}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ borderBottomWidth: 0, paddingTop: 10, flex: 1, marginRight: 15 }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >State:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.state_name}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 0, paddingTop: 10, flex: 1, }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >City:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.city_name}</Text>
                                    </View>
                                </View>
                            </View>

                            {/* Contact Person Details */}
                            <View style={{ ...shadow, borderRadius: 8, marginBottom: 20, paddingBottom: 15 }}>
                                <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, }}>
                                    <Text style={{ fontFamily: font.bold, color: color.primeColor }}>Contact Person Details</Text>
                                </View>
                                <FlatList
                                    scrollEnabled={false}
                                    contentContainerStyle={{ paddingHorizontal: 15 }}
                                    data={data.lead_contacts}
                                    renderItem={({ item, index }) =>
                                        <View key={index + ''} style={{ paddingVertical: 12, borderBottomWidth: data.lead_contacts.length - 1 == index ? 0 : 1, paddingBottom: data.lead_contacts.length - 1 == index ? 0 : 12 }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Text style={{ flex: 1, fontFamily: font.bold }}>{item.cperson_name}</Text>
                                                <TouchableOpacity onPress={() => this.whatsappSend(item)} style={{ marginRight: 12 }}>
                                                    <Image source={require('../../assets/img/whatsapp.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                                </TouchableOpacity>
                                                {item.cperson_email ? <TouchableOpacity onPress={() => Linking.openURL('mailto:' + item.cperson_email)} style={{ marginRight: 12 }}>
                                                    <Image source={require('../../assets/img/email.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                                </TouchableOpacity> : null}
                                                <TouchableOpacity onPress={() => Linking.openURL('tel:' + item.cperson_mobile)} >
                                                    <Image source={require('../../assets/img/phone-call.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                                <View style={{ flex: 1 }}>
                                                    {item.cperson_email ? <Text style={{ fontFamily: font.reg, marginBottom: 5 }}>{item.cperson_email}</Text> : null}
                                                    <Text style={{ fontFamily: font.reg, }}>{item.cperson_mobile}</Text>
                                                </View>
                                                <TouchableOpacity onPress={() => this.setState({ shareContact: item, shareVisible: true })} style={{ marginRight: 12 }}>
                                                    <Image source={require('../../assets/img/share.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => this.saveContact(item)}>
                                                    <Image source={require('../../assets/img/save.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    }
                                />
                            </View>

                            {/* Lead Details */}
                            <View style={{ ...shadow, borderRadius: 8, marginBottom: 20, paddingBottom: 15 }}>
                                <View style={{ paddingVertical: 8, paddingHorizontal: 15, backgroundColor: color.primeColor_op_20, borderTopLeftRadius: 7, borderTopRightRadius: 7, flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ flex: 1, fontFamily: font.bold, color: color.primeColor }}>Lead Details</Text>
                                    <View style={{ backgroundColor: this.checkStatus(data.status).color, paddingHorizontal: 7, paddingVertical: 1, borderRadius: 3 }}>
                                        <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 12 }}>{this.checkStatus(data.status).name}</Text>
                                    </View>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Lead Date:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.idate}</Text>
                                </View>

                                <View style={{ marginHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 10, flex: 1, marginRight: 15 }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Generated By:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.gen_by_name}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 10, flex: 1, }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Assign To:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.assign_to_name}</Text>
                                    </View>
                                </View>

                                <View style={{ marginHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 10, flex: 1, marginRight: 15 }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Monitor By:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.monitor_by_name}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 10, flex: 1, }}>
                                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Lead Source:</Text>
                                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{data.inq_source_name}</Text>
                                    </View>
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Products/Services:</Text>
                                    {data.product_name ?
                                        data.product_name.split(',').map((item, index) =>
                                            <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >{index + 1})  {item.trim()}</Text>
                                        )
                                        : null
                                    }
                                </View>

                                <View style={{ marginHorizontal: 15, borderBottomWidth: 0, paddingTop: 10, borderBottomWidth: data.received_order_amount ? 1 : 0, paddingBottom: data.received_order_amount ? 10 : 0 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Approx Amount:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >Rs. {data.approx_amount}</Text>
                                </View>

                                {data.received_order_amount ? <View style={{ marginHorizontal: 15, borderBottomWidth: 0, paddingTop: 10 }}>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Received Order Amount:</Text>
                                    <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >Rs. {data.received_order_amount}</Text>
                                </View> : null}

                                {/* <View style={{ marginHorizontal: 15, borderBottomWidth: 1, paddingVertical: 10 }}>
                        <Text style={{ fontFamily: font.reg, color: color.lableColor }} >Remarks:</Text>
                        <Text style={{ fontFamily: font.reg, marginTop: 3, fontSize: 15 }} >Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</Text>
                    </View> */}
                            </View>
                        </View>}
                </ScrollView>

                {data.length != 0 && data.status_array.length != 0 ? <TouchableOpacity onPress={() => this.setState({ changeStatusModalVisible: true })} style={{ height: 35, position: 'absolute', left: 15, right: 15, bottom: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 5 }}>
                    <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>CHANGE STATUS</Text>
                </TouchableOpacity> : null}

                {/* Share Modal */}
                <Modal
                    transparent
                    animationType="fade"
                    visible={shareVisible}
                    onRequestClose={() => this.setState({ shareVisible: false })}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#0005' }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        <SafeAreaView style={{ backgroundColor: '#0000' }} />
                        <TouchableOpacity onPress={() => this.setState({ shareVisible: false })} activeOpacity={1} style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 20, justifyContent: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ ...shadow, borderRadius: 8, overflow: 'scroll', }}>
                                    {/* <View style={{ backgroundColor: color.primeColor_op_20, height: 35, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 7, borderTopRightRadius: 7 }}>
                                        <Text style={{ fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>Change Status</Text>
                                        <TouchableOpacity onPress={() => this.setState({ shareVisible: false })} style={{ position: 'absolute', height: 35, width: 35, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                                            <Image source={require('../../assets/img/cancle_icon.png')} style={{ tintColor: '#000', height: '40%', width: '40%', resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View> */}
                                    <View style={{ paddingBottom: 10 }}>

                                        <FlatList
                                            bounces={false}
                                            contentContainerStyle={{}}
                                            data={shareDocumentsList}
                                            renderItem={({ item, index }) =>
                                                <View style={{ borderBottomWidth: 1, }}>
                                                    <TouchableOpacity onPress={() => { shareDocumentsList[index].isSelected = !item.isSelected; this.setState({ shareDocumentsList }) }} style={{ paddingVertical: 15, paddingHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                                                        <Text style={{ fontFamily: font.bold, flex: 1, color: item.isSelected ? '#000' : color.lableColor }}>{item.name}</Text>
                                                        {item.isSelected && <Image source={require('../../assets/img/check-mark.png')} style={{ height: 15, width: 15, resizeMode: 'contain', tintColor: color.primeColor }} />}
                                                    </TouchableOpacity>
                                                </View>
                                            }
                                        />

                                        <TouchableOpacity onPress={() => this.share()} style={{ height: 35, marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, marginVertical: 10, marginTop: 20 }}>
                                            <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>SHARE</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                </Modal>

                {/* Change Status */}
                <Modal
                    transparent
                    animationType="fade"
                    visible={changeStatusModalVisible}
                    onRequestClose={() => this.setState({ changeStatusModalVisible: false })}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#0005' }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        <SafeAreaView style={{ backgroundColor: '#0000' }} />
                        <TouchableOpacity onPress={() => this.setState({ changeStatusModalVisible: false })} activeOpacity={1} style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 20, justifyContent: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ ...shadow, borderRadius: 8, overflow: 'scroll', }}>
                                    <View style={{ backgroundColor: color.primeColor_op_20, height: 35, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 7, borderTopRightRadius: 7 }}>
                                        <Text style={{ fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>Change Status</Text>
                                        <TouchableOpacity onPress={() => this.setState({ changeStatusModalVisible: false })} style={{ position: 'absolute', height: 35, width: 35, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                                            <Image source={require('../../assets/img/cancle_icon.png')} style={{ tintColor: '#000', height: '40%', width: '40%', resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View>
                                    <ScrollView bounces={false} contentContainerStyle={{ paddingVertical: 15 }}>

                                        <Picker
                                            disabled={false}
                                            placeholder="Lead Status"
                                            mainViewStyle={{ marginBottom: 15, }}
                                            data={leadStatusList}
                                            value={leadStatus.name}
                                            onSelect={leadStatus => this.setState({ leadStatus })}
                                        />

                                        {leadStatus.id === '3' ? <>
                                            <DatePicker
                                                mainViewStyle={{ marginBottom: 15 }}
                                                placeholder="Date"
                                                maxDate={new Date()}
                                                value={date}
                                                onChange={date => this.setState({ date })} />

                                            <View style={{ marginBottom: 15 }}>
                                                <FloatingLabelInput
                                                    keyboardType="numeric"
                                                    label="Amount"
                                                    maxLength={7}
                                                    value={order_amount}
                                                    onChangeText={order_amount => this.setState({ order_amount })}
                                                />
                                            </View>
                                        </> : null}

                                        {leadStatus.id === '4' ? <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="sentences"
                                                multiline={true}
                                                style={{ height: 90, textAlignVertical: 'top' }}
                                                blurOnSubmit={false}
                                                label="Reason"
                                                value={reason}
                                                onChangeText={reason => this.setState({ reason })}
                                            />
                                        </View> : null}

                                        <TouchableOpacity onPress={() => this.change()} disabled={isStatusChangeLoading} style={{ height: 35, marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, marginVertical: 10 }}>
                                            {isStatusChangeLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>CHANGE</Text>}
                                        </TouchableOpacity>
                                    </ScrollView>
                                </View>
                            </TouchableWithoutFeedback>
                        </TouchableOpacity>
                        <SnackBar ref="SnackBar" />
                    </KeyboardAvoidingView>
                </Modal>
                <SnackBar ref="SnackBarMain" />
            </View>
        );
    }
}
