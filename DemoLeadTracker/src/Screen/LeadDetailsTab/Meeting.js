import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, RefreshControl, ActivityIndicator, Keyboard } from 'react-native';
import moment from 'moment';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import { color, font, shadow } from '../../Component/Styles';
import FloatingLabelInput from '../../Component/FloatingLabelInput';
import Picker from '../../Component/Picker';
import DatePicker from '../../Component/DatePicker';
import SnackBar from '../../Component/SnackBar';
import CustomAlert from '../../Component/CustomAlert';
import Global from '../../Global';
import { API } from '../../Privet';

export default class Meeting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAddLoading: false,
            addModalVisible: false,
            meeting_id: '',
            employee: { employee_id: Global.main.state.id, employee_name: Global.main.state.fname + ' ' + Global.main.state.lname },
            date: new Date(),
            from_time: null,
            to_time: null,
            remarks: '',
            employeeList: [],
        };
    }

    componentDidMount() {
        this.employeeList();
    }

    // employeeList
    employeeList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_generated_by, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ employeeList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    add() {
        Keyboard.dismiss();
        const { meeting_id, date, employee, remarks, from_time, to_time } = this.state;
        const { id, urole } = Global.main.state;

        if (!remarks) {
            this.refs.SnackBar.show('Please enter remarks.');
        }
        else if (!from_time) {
            this.refs.SnackBar.show('Please select from time.');
        }
        else if (!to_time) {
            this.refs.SnackBar.show('Please select to time.');
        }
        else {
            this.setState({ isAddLoading: true });
            const data = {
                user_id: id,
                utype: urole,
                lead_id: this.props.lead_id,
                executive_id: employee.employee_id,
                mdate: moment(date).format('DD-MM-YYYY'),
                from_time: moment(from_time).format('LT'),
                to_time: moment(to_time).format('LT'),
                remarks: remarks,
                lead_meeting_id: meeting_id,
            }

            fetch(meeting_id ? API.edit_lead_meeting : API.add_lead_meeting, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    console.log(res);
                    this.setState({ isAddLoading: false });
                    if (res.status) {
                        this.clean();
                        this.props.onRefresh();
                    } else {
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isAddLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    async edit(index, item) {
        await this._menu[index].hide();
        await setTimeout(() => {
            this.setState({
                meeting_id: item.id,
                employee: { employee_id: item.ex_id, employee_name: item.exname },
                date: new Date(moment(item.date, 'DD-MM-YYYY')),
                from_time: new Date(moment(item.from_time, 'HH:mm:ss')),
                to_time: new Date(moment(item.to_time, 'HH:mm:ss')),
                remarks: item.mremarks,
                addModalVisible: true,
            })
        }, 300);
    }

    async remove(index, item) {
        await this._menu[index].hide();
        setTimeout(() => {
            this.refs.CustomAlert.onShow('Are you sure you want to delete this meeting?', '', item.id)
        }, 300);
    }

    delete(_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            lead_meeting_id: _id
        }

        fetch(API.delete_lead_meeting, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isAddLoading: false });
                if (res.status) {
                    this.props.onRefresh();
                } else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                this.setState({ isAddLoading: false });
                console.log('error: ', error);
            });
    }

    _menu = [];
    displayDropdown = (index, item) => {
        return (
            <Menu ref={(menu) => { this._menu[index] = menu }} >
                <MenuItem textStyle={{ fontFamily: font.mid }} onPress={() => this.edit(index, item)}>Edit</MenuItem>
                <MenuItem textStyle={{ fontFamily: font.mid }} onPress={() => this.remove(index, item)}>Remove</MenuItem>
            </Menu>
        )
    }

    clean() {
        this.setState({
            isAddLoading: false,
            addModalVisible: false,
            meeting_id: '',
            employee: { employee_id: Global.main.state.id, employee_name: Global.main.state.fname + ' ' + Global.main.state.lname },
            date: new Date(),
            from_time: null,
            to_time: null,
            remarks: '',
        })
    }

    render() {
        const { data = [], isLoading, onRefresh } = this.props;
        const { isAddLoading, addModalVisible, meeting_id, employeeList, employee, date, to_time, from_time, remarks } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingTop: 15, paddingBottom: 75 }}
                    data={data}
                    renderItem={({ item, index }) =>
                        <View style={{ ...shadow, padding: 10, marginBottom: 15, borderRadius: 8 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1, marginRight: 10 }}>
                                    <Text style={{ fontFamily: font.bold, fontSize: 15, color: color.primeColor }}>{item.exname}</Text>
                                </View>
                                <TouchableOpacity onPress={() => this._menu[index].show()} style={{ paddingVertical: 4 }}>
                                    <Image source={require('../../assets/img/more_icon.png')} style={{ height: 17, width: 17, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                {this.displayDropdown(index, item)}
                            </View>
                            {/* <Text style={{ marginTop: 3, fontFamily: font.reg, color: color.lableColor, fontSize: 13 }}>{moment().format('DD-MM-YYYY on LT')}</Text> */}
                            <Text style={{ marginTop: 3, fontFamily: font.reg, color: color.lableColor, }}>Date:  <Text style={{ color: '#000' }}>{item.date}</Text></Text>
                            <Text style={{ marginTop: 3, fontFamily: font.reg, color: color.lableColor, }}>From Time:  <Text style={{ color: '#000' }}>{moment(item.from_time, 'HH:mm:ss').format('LT')}</Text></Text>
                            <Text style={{ marginTop: 3, fontFamily: font.reg, color: color.lableColor, }}>To Time:  <Text style={{ color: '#000' }}>{moment(item.to_time, 'HH:mm:ss').format('LT')}</Text></Text>

                            <Text style={{ fontFamily: font.reg, marginVertical: 8 }}>{item.mremarks}</Text>
                        </View>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Lead meeting not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => onRefresh && onRefresh()} />}
                />

                <TouchableOpacity onPress={() => this.setState({ addModalVisible: true })} style={{ ...shadow, borderRadius: 50, position: 'absolute', alignSelf: 'center', alignItems: 'center', bottom: 20, flexDirection: 'row', padding: 7 }}>
                    <View style={{ height: 35, width: 35, borderRadius: 50, backgroundColor: color.primeColor, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/img/cancle_icon.png')} style={{ height: '40%', width: '40%', resizeMode: 'contain', tintColor: '#fff', transform: [{ rotate: '45deg' }] }} />
                    </View>
                    <Text style={{ fontFamily: font.bold, paddingHorizontal: 10, fontSize: 17, color: color.primeColor }}>Add Meeting</Text>
                </TouchableOpacity>

                {/* Add Meeting */}
                <Modal
                    transparent
                    animationType="fade"
                    visible={addModalVisible}
                    onRequestClose={() => this.clean()}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#0005' }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        <SafeAreaView style={{ backgroundColor: '#0000' }} />
                        <TouchableOpacity onPress={() => this.clean()} activeOpacity={1} style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 20, justifyContent: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ ...shadow, borderRadius: 8, overflow: 'scroll', }}>
                                    <View style={{ backgroundColor: color.primeColor_op_20, height: 35, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 7, borderTopRightRadius: 7 }}>
                                        <Text style={{ fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>{meeting_id ? 'Edit Meeting' : 'Add Meeting'}</Text>
                                        <TouchableOpacity onPress={() => this.clean()} style={{ position: 'absolute', height: 35, width: 35, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                                            <Image source={require('../../assets/img/cancle_icon.png')} style={{ tintColor: '#000', height: '40%', width: '40%', resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View>
                                    <ScrollView bounces={false} contentContainerStyle={{ paddingVertical: 15 }}>

                                        <DatePicker
                                            mainViewStyle={{ marginBottom: 15 }}
                                            placeholder="Date"
                                            value={date}
                                            onChange={date => this.setState({ date })} />

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <DatePicker
                                                mode="time"
                                                mainViewStyle={{ flex: 1, marginRight: 15 }}
                                                placeholder="From Time"
                                                value={from_time}
                                                onChange={from_time => this.setState({ from_time })} />
                                            <DatePicker
                                                mode="time"
                                                mainViewStyle={{ flex: 1, marginLeft: 0 }}
                                                placeholder="To Time"
                                                value={to_time}
                                                onChange={to_time => this.setState({ to_time })} />
                                        </View>

                                        <Picker
                                            disabled={false}
                                            placeholder="Employee"
                                            mainViewStyle={{ marginBottom: 15, }}
                                            data={employeeList}
                                            nameKey="employee_name"
                                            value={employee.employee_name}
                                            onSelect={employee => this.setState({ employee })}
                                        />

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="sentences"
                                                multiline={true}
                                                style={{ height: 90, textAlignVertical: 'top' }}
                                                blurOnSubmit={false}
                                                label="Remarks"
                                                value={remarks}
                                                onChangeText={remarks => this.setState({ remarks })}
                                            />
                                        </View>

                                        <TouchableOpacity onPress={() => this.add()} disabled={isAddLoading} style={{ height: 35, marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, marginVertical: 10 }}>
                                            {isAddLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>{meeting_id ? 'EDIT' : 'ADD'}</Text>}
                                        </TouchableOpacity>
                                    </ScrollView>
                                </View>
                            </TouchableWithoutFeedback>
                        </TouchableOpacity>
                        <SnackBar ref="SnackBar" />
                    </KeyboardAvoidingView>
                </Modal>
                <CustomAlert ref="CustomAlert" onPressOk={(id) => this.delete(id)} cancel />
            </View>
        );
    }
}
