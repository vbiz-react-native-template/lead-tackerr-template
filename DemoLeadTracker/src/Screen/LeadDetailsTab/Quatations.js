import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Linking, RefreshControl, ActivityIndicator, Keyboard } from 'react-native';
import moment from 'moment';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import DocumentPicker from 'react-native-document-picker';
import { color, font, shadow } from '../../Component/Styles';
import FloatingLabelInput from '../../Component/FloatingLabelInput';
import Picker from '../../Component/Picker';
import CustomAlert from '../../Component/CustomAlert';
import DatePicker from '../../Component/DatePicker';
import SnackBar from '../../Component/SnackBar';
import Global from '../../Global';
import { API } from '../../Privet';

export default class Quatations extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAddLoading: false,
            addModalVisible: false,
            quotation_id: '',
            employee: { employee_id: Global.main.state.id, employee_name: Global.main.state.fname + ' ' + Global.main.state.lname },
            date: new Date(),
            amount: '',
            remarks: '',
            quotation_attachment: [],
            employeeList: [],
        };
    }

    componentDidMount() {
        this.employeeList();
    }

    // employeeList
    employeeList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_generated_by, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ employeeList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    async selectFile() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            this.setState({ quotation_attachment: res })
            // console.log(res);
        } catch (err) {
            console.log('selectFile error: ', err)
        }
    }

    add() {
        Keyboard.dismiss();
        const { quotation_id, date, employee, amount, remarks, quotation_attachment } = this.state;
        const { id, urole } = Global.main.state;

        if (!remarks) {
            this.refs.SnackBar.show('Please enter remarks.');
        }
        else if (!amount) {
            this.refs.SnackBar.show('Please enter amount.');
        }
        else if (!remarks) {
            this.refs.SnackBar.show('Please enter remarks.');
        }
        else {
            this.setState({ isAddLoading: true });

            let formdata = new FormData();

            formdata.append('utype', urole);
            formdata.append('user_id', id);
            formdata.append('qdate', moment(date).format('DD-MM-YYYY'));
            formdata.append('qamount', amount);
            formdata.append('remarks', remarks);
            formdata.append('qremarks', remarks);
            formdata.append('executive_id', employee.employee_id);
            formdata.append('lead_id', this.props.lead_id);
            formdata.append('attachment', quotation_attachment.uri ? { uri: quotation_attachment.uri, name: quotation_attachment.name, type: quotation_attachment.type } : '');
            formdata.append('lead_quotation_id', quotation_id);

            console.log(formdata);

            fetch(quotation_id ? API.edit_lead_quotation : API.add_lead_quotation, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'multipart/form-data'
                },
                body: formdata
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    this.setState({ isAddLoading: false });
                    if (res.status) {
                        this.clean();
                        this.props.onRefresh();
                    } else {
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isAddLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    async edit(index, item) {
        await this._menu[index].hide();
        await setTimeout(() => {
            this.setState({
                quotation_id: item.id,
                employee: { employee_id: item.ex_id, employee_name: item.exname },
                date: new Date(moment(item.qdate, 'DD-MM-YYYY')),
                amount: item.qamount.replace(/,/g, ''),
                remarks: item.qremarks,
                addModalVisible: true,
            })
        }, 300);
    }

    async remove(index, item) {
        await this._menu[index].hide();
        setTimeout(() => {
            this.refs.CustomAlert.onShow('Are you sure you want to delete this meeting?', '', item.id)
        }, 300);
    }

    delete(_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            lead_quotation_id: _id
        }

        fetch(API.delete_lead_quotation, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isAddLoading: false });
                if (res.status) {
                    this.props.onRefresh();
                } else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                this.setState({ isAddLoading: false });
                console.log('error: ', error);
            });
    }

    _menu = [];
    displayDropdown = (index, item) => {
        return (
            <Menu ref={(menu) => { this._menu[index] = menu }} >
                <MenuItem textStyle={{ fontFamily: font.mid }} onPress={() => this.edit(index, item)}>Edit</MenuItem>
                <MenuItem textStyle={{ fontFamily: font.mid }} onPress={() => this.remove(index, item)}>Remove</MenuItem>
            </Menu>
        )
    }

    clean() {
        this.setState({
            isAddLoading: false,
            addModalVisible: false,
            quotation_id: '',
            employee: { employee_id: Global.main.state.id, employee_name: Global.main.state.fname + ' ' + Global.main.state.lname },
            date: new Date(),
            amount: '',
            remarks: '',
            quotation_attachment: [],
        })
    }

    render() {
        const { data = [], isLoading, onRefresh } = this.props;
        const { isAddLoading, addModalVisible, quotation_id, employeeList, employee, amount, date, remarks, quotation_attachment } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingTop: 15, paddingBottom: 75 }}
                    data={data}
                    renderItem={({ item, index }) =>
                        <View style={{ ...shadow, padding: 10, marginBottom: 15, borderRadius: 8, overflow: 'hidden' }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ flex: 1, marginRight: 10 }}>
                                    <Text style={{ fontFamily: font.bold, fontSize: 15, color: color.primeColor }}>{item.exname}</Text>
                                    <Text style={{ marginTop: 3, fontFamily: font.reg, color: color.lableColor, fontSize: 13 }}>{item.qdate}</Text>
                                </View>
                                <TouchableOpacity onPress={() => this._menu[index].show()} style={{ paddingVertical: 4 }}>
                                    <Image source={require('../../assets/img/more_icon.png')} style={{ height: 17, width: 17, resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                {this.displayDropdown(index, item)}
                                {/* <Text style={{ marginTop: 3, fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>Rs. {item.qamount ? item.qamount : '0'}</Text> */}
                            </View>
                            <Text style={{ fontFamily: font.reg, marginVertical: 8 }}>{item.qremarks}</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                                <TouchableOpacity onPress={() => Linking.openURL(item.qattach)} style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
                                    <Text style={{ fontFamily: font.bold, fontSize: 13 }}>View File</Text>
                                    <Image source={require('../../assets/img/back.png')} style={{ marginLeft: 10, height: 13, width: 13, resizeMode: 'contain', transform: [{ rotate: '180deg' }] }} />
                                </TouchableOpacity>

                                <Text style={{ marginTop: 3, fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>Rs. {item.qamount ? item.qamount : '0'}</Text>
                            </View>
                        </View>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Lead quotations not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => onRefresh && onRefresh()} />}
                />

                <TouchableOpacity onPress={() => this.setState({ addModalVisible: true })} style={{ ...shadow, borderRadius: 50, position: 'absolute', alignSelf: 'center', alignItems: 'center', bottom: 20, flexDirection: 'row', padding: 7 }}>
                    <View style={{ height: 35, width: 35, borderRadius: 50, backgroundColor: color.primeColor, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/img/cancle_icon.png')} style={{ height: '40%', width: '40%', resizeMode: 'contain', tintColor: '#fff', transform: [{ rotate: '45deg' }] }} />
                    </View>
                    <Text style={{ fontFamily: font.bold, paddingHorizontal: 10, fontSize: 17, color: color.primeColor }}>Add Quotations</Text>
                </TouchableOpacity>

                {/* Add Quatations */}
                <Modal
                    transparent
                    animationType="fade"
                    visible={addModalVisible}
                    onRequestClose={() => this.clean()}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#0005' }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        <SafeAreaView style={{ backgroundColor: '#0000' }} />
                        <TouchableOpacity onPress={() => this.clean()} activeOpacity={1} style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 20, justifyContent: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ ...shadow, borderRadius: 8, overflow: 'scroll', }}>
                                    <View style={{ backgroundColor: color.primeColor_op_20, height: 35, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 7, borderTopRightRadius: 7 }}>
                                        <Text style={{ fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>{quotation_id ? 'Edid Quotations' : 'Add Quotations'}</Text>
                                        <TouchableOpacity onPress={() => this.clean()} style={{ position: 'absolute', height: 35, width: 35, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                                            <Image source={require('../../assets/img/cancle_icon.png')} style={{ tintColor: '#000', height: '40%', width: '40%', resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View>
                                    <ScrollView bounces={false} contentContainerStyle={{ paddingVertical: 15 }}>

                                        <DatePicker
                                            mainViewStyle={{ marginBottom: 15 }}
                                            placeholder="Date"
                                            maxDate={new Date()}
                                            value={date}
                                            onChange={date => this.setState({ date })} />

                                        <Picker
                                            disabled={false}
                                            placeholder="Employee"
                                            mainViewStyle={{ marginBottom: 15, }}
                                            data={employeeList}
                                            nameKey="employee_name"
                                            value={employee.employee_name}
                                            onSelect={employee => this.setState({ employee })}
                                        />

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                keyboardType="numeric"
                                                label="Amount"
                                                maxLength={7}
                                                value={amount}
                                                onChangeText={amount => this.setState({ amount })}
                                            />
                                        </View>

                                        <View style={{ borderRadius: 8, marginHorizontal: 15, marginBottom: 15, borderWidth: 1, borderStyle: "dashed", marginTop: 8 }}>
                                            <TouchableOpacity onPress={() => this.selectFile()} style={{ height: 60 }}>
                                                {quotation_attachment.uri ?
                                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 8 }}>
                                                        <Image source={require('../../assets/img/file.png')} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                                                        <Text numberOfLines={1} style={{ flex: 1, fontFamily: font.reg, marginLeft: 10 }}>{quotation_attachment.name}</Text>
                                                    </View>
                                                    : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={{ fontFamily: font.bold, color: color.borderColor }}>Quotation Attachment</Text>
                                                    </View>}
                                            </TouchableOpacity>
                                        </View>

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="sentences"
                                                multiline={true}
                                                style={{ height: 90, textAlignVertical: 'top' }}
                                                blurOnSubmit={false}
                                                label="Remarks"
                                                value={remarks}
                                                onChangeText={remarks => this.setState({ remarks })}
                                            />
                                        </View>

                                        <TouchableOpacity onPress={() => this.add()} disabled={isAddLoading} style={{ height: 35, marginHorizontal: 15, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, marginVertical: 10 }}>
                                            {isAddLoading ? <ActivityIndicator color="#fff" /> : <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>{quotation_id ? 'EDIT' : 'ADD'}</Text>}
                                        </TouchableOpacity>
                                    </ScrollView>
                                </View>
                            </TouchableWithoutFeedback>
                        </TouchableOpacity>
                        <SnackBar ref="SnackBar" />
                    </KeyboardAvoidingView>
                </Modal>
                <CustomAlert ref="CustomAlert" onPressOk={(id) => this.delete(id)} cancel />
            </View>
        );
    }
}
