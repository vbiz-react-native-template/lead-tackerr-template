import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, ActivityIndicator, RefreshControl } from 'react-native';
import moment from 'moment';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import Picker from '../Component/Picker';
import DatePicker from '../Component/DatePicker';
import CustomAlert from '../Component/CustomAlert';
import { API } from '../Privet';
import Global from '../Global';

export default class Lead extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isFilter: false,
            totalPage: 1,
            page: 1,
            leadList: [],

            filterModalVisible: false,

            employeeList: [],
            products_servicesList: [],
            countryList: [],
            stateList: [],
            cityList: [],
            leadStatusList: [{ id: '0', name: 'Cold' }, { id: '1', name: 'Warm' }, { id: '2', name: 'Hot' }, { id: '3', name: 'Order Received' }, { id: '4', name: 'Order Loss' }],
            leadSourceList: [],

            c_name: '',
            o_name: '',
            o_mobile: '',
            name: '',
            mobile: '',
            products_services: [],
            generated_by: [],
            assign_to: [],
            monitor_by: [],
            lead_source: [],
            lead_status: [],
            country: [],
            state: [],
            city: [],
            from_date: null,
            to_date: null,
        };
    }

    async componentDidMount() {
        const { route } = this.props;
        // console.log(route)
        if (route.params && route.params.lead_status) {
            this.setState({ lead_status: route.params.lead_status, isFinite: true }, () => this.filterLeadList())
        } else {
            this.leadList();
        }
        this.employeeList();
        this.countryList()
        this.products_servicesList();
        this.leadSourceList();
    }

    // employeeList
    employeeList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_generated_by, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ employeeList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // countryList
    countryList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.country_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ countryList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // stateList
    stateList(country_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            country_id: country_id.id
        }

        fetch(API.state_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('stateList: ', res)
                if (res.status) {
                    this.setState({ stateList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // cityList
    cityList(state_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            state_id: state_id.id
        }

        fetch(API.city_list, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log('ciryList: ', res)
                if (res.status) {
                    this.setState({ cityList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // products_servicesList
    products_servicesList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_products_services, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                if (res.status) {
                    this.setState({ products_servicesList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    delete(_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            lead_id: _id
        }

        fetch(API.delete_lead, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                if (res.status) {
                    this.setState({ isLoading: true, page: 1, totalPage: 1, isFilter: false }, () => { this.clean(); this.leadList(); })
                } else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // leadSourceList
    leadSourceList() {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole
        }

        fetch(API.get_lead_source, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res)
                if (res.status) {
                    this.setState({ leadSourceList: res.data, })
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    // lead List
    leadList() {
        const { isLoading, leadList, totalPage, page } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isFilter: false, });

        if (totalPage >= page) {
            const data = {
                user_id: id,
                utype: urole,
                page: page
            }

            // console.log(data);

            fetch(API.get_all_lead, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    if (res.status) {
                        if (isLoading || leadList.length == 0) {
                            this.setState({ isLoading: false, leadList: res.data, totalPage: res.totalpage, page: page + 1 })
                        } else {
                            this.setState({ isLoading: false, leadList: [...leadList, ...res.data], totalPage: res.totalpage, page: page + 1 })
                        }
                    }
                    else {
                        this.setState({ isLoading: false });
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    // lead List
    filterLeadList() {
        this.setState({ isFilter: true, })
        const { isLoading, leadList, totalPage, page, c_name, o_name, o_mobile, name, mobile, products_services, generated_by,
            assign_to,
            monitor_by,
            lead_source,
            lead_status,
            country,
            state,
            city,
            from_date,
            to_date, } = this.state;
        const { id, urole } = Global.main.state;

        if (totalPage >= page) {
            const data = {
                user_id: id,
                utype: urole,
                page: page,
                search_company: c_name,
                search_cmobile: mobile,
                search_cname: name,
                search_owner_mobile: o_mobile,
                search_owner_name: o_name,
                search_source: lead_source.id ? lead_source.id : '',
                search_product: products_services.id ? products_services.id : '',
                search_country_id: country.id ? country.id : '',
                search_state_id: state.id ? state.id : '',
                search_city_id: city.id ? city.id : '',
                search_generated_by: generated_by.employee_id ? generated_by.employee_id : '',
                search_monitor_by: monitor_by.employee_id ? monitor_by.employee_id : '',
                search_assign_to: assign_to.employee_id ? assign_to.employee_id : '',
                search_status: lead_status.id ? lead_status.id : '',
                search_from: from_date ? from_date : '',
                search_to: to_date ? to_date : '',
            }

            // console.log(data);

            fetch(API.search_lead, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    if (res.status) {
                        if (isLoading || leadList.length == 0) {
                            this.setState({ isLoading: false, leadList: res.data, totalPage: res.totalpage, page: page + 1 })
                        } else {
                            this.setState({ isLoading: false, leadList: [...leadList, ...res.data], totalPage: res.totalpage, page: page + 1 })
                        }
                    }
                    else {
                        this.setState({ leadList: [], isLoading: false });
                        // this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    renderFooter = () => {
        const { totalPage, page, isLoading, leadList } = this.state;
        if (page <= totalPage && !isLoading && leadList.length != 0) {

            return (
                <View>
                    <ActivityIndicator color={color.primeColor} style={{ margin: 20 }} />
                </View>
            )
        } else {
            return null
        }
    }

    checkStatus(status) {
        if (status === '0') {
            return ({ name: 'COLD', color: color.cold })
        }
        else if (status === '1') {
            return ({ name: 'WARM', color: color.warm })
        }
        else if (status === '2') {
            return ({ name: 'HOT', color: color.hot })
        }
        else if (status === '3') {
            return ({ name: 'RECEIVED', color: color.green })
        }
        else if (status === '4') {
            return ({ name: 'LOSS', color: color.red })
        }
        else {
            return ({ name: '--', color: '#0000000' })
        }
    }

    clean() {
        this.setState({
            c_name: '',
            o_name: '',
            o_mobile: '',
            name: '',
            mobile: '',
            products_services: [],
            generated_by: [],
            assign_to: [],
            monitor_by: [],
            lead_source: [],
            lead_status: [],
            country: [],
            state: [],
            city: [],
            from_date: null,
            to_date: null,
        })
    }

    render() {
        const { isLoading, isFilter, leadList, filterModalVisible, products_servicesList, employeeList, stateList, countryList, leadStatusList, cityList, leadSourceList,
            c_name,
            o_name,
            o_mobile,
            name,
            mobile,
            products_services,
            generated_by,
            assign_to,
            monitor_by,
            lead_source,
            lead_status,
            country,
            state,
            city,
            from_date,
            to_date, } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>Lead</Text>

                    <TouchableOpacity onPress={() => this.setState({ filterModalVisible: true })} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/filter.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                </View>

                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingTop: 15, paddingBottom: 55 + 20 }}
                    data={leadList}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity key={index + ''} onPress={() => this.props.navigation.navigate('LeadDetails', { lead_id: item.lead_id })} style={{ ...shadow, marginBottom: 15, padding: 15, borderRadius: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                <View style={{ height: 55, width: 55, backgroundColor: this.checkStatus(item.status).color + '11', borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: font.bold, color: this.checkStatus(item.status).color }}>{item.company.charAt(0).toUpperCase()}</Text>
                                </View>
                                <View style={{ flex: 1, paddingHorizontal: 12 }}>
                                    <Text numberOfLines={2} style={{ fontFamily: font.bold, fontSize: 15 }}>{item.company}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, fontSize: 13 }}>{item.owner_name}</Text>
                                </View>
                                <View style={{ backgroundColor: this.checkStatus(item.status).color, paddingHorizontal: 7, paddingVertical: 1, borderRadius: 3 }}>
                                    <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 12 }}>{this.checkStatus(item.status).name}</Text>
                                </View>
                            </View>
                            {/* <View style={{ marginTop: 8 }}>
                                <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, }}><Text style={{ color: color.lableColor }}>Mobile No.: </Text> {item.mobile}</Text>
                                <Text numberOfLines={2} style={{ fontFamily: font.reg, fontSize: 12, marginTop: 3, color: color.lableColor }}>{moment().format('DD-MM-YYYY')} at {moment().format('LT')}</Text>
                            </View> */}
                            <View style={{ marginTop: 12, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Gen. By:  </Text>
                                <Text style={{ fontFamily: font.reg, }}>{item.gen_by_name}</Text>
                            </View>
                            <View style={{ marginTop: 12, flexDirection: 'row' }}>
                                <View style={{ flex: 1, }}>
                                    <Text style={{ fontFamily: font.reg, marginBottom: 6 }}>{item.assign_to_name}</Text>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Assign To</Text>
                                </View>
                                {/* <View style={{ height: '100%', width: 1.5, backgroundColor: color.borderColor }} /> */}
                                <View style={{ flex: 1, }}>
                                    <Text style={{ fontFamily: font.reg, marginBottom: 6 }}>{item.monitor_by_name}</Text>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Monitor By</Text>
                                </View>
                            </View>

                            <View style={{ marginTop: 12, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: font.reg, color: color.lableColor, fontSize: 13, flex: 1, }}>{item.lead_date}</Text>
                                {/* <Image source={require('../assets/img/back.png')} style={{ height: 14, width: 14, tintColor: color.lableColor, transform: [{ rotate: '180deg' }] }} /> */}

                                <TouchableOpacity onPress={() => this.refs.CustomAlert.onShow('Are you sure you want to delete this lead?', '', item.lead_id)} style={{ height: 28, width: 28, borderRadius: 100, backgroundColor: color.borderColor, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image source={require('../assets/img/delete.png')} style={{ height: '45%', width: '45%', resizeMode: 'contain', tintColor: color.lableColor}} />
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Lead not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.setState({ isLoading: true, page: 1, totalPage: 1, isFilter: false }, () => { this.clean(); this.leadList(); })} />}
                    onEndReached={() => isFilter ? this.filterLeadList() : this.leadList()}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={this.renderFooter}
                />

                <TouchableOpacity onPress={() => this.props.navigation.navigate('AddLead')} style={{ height: 55, width: 55, backgroundColor: color.primeColor, position: 'absolute', bottom: 20, right: 20, borderTopLeftRadius: 55, borderTopEndRadius: 55, borderBottomStartRadius: 55, justifyContent: 'center', alignItems: 'center' }} >
                    <Image source={require('../assets/img/cancle_icon.png')} style={{ height: '40%', width: '40%', transform: [{ rotate: '45deg' }], tintColor: '#fff' }} />
                </TouchableOpacity>

                {/* Filter Modal */}
                <Modal
                    transparent
                    animationType="fade"
                    visible={filterModalVisible}
                    onRequestClose={() => this.setState({ filterModalVisible: false })}>
                    <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#0005' }} behavior={Platform.OS === 'ios' ? 'padding' : null}>
                        <SafeAreaView style={{ backgroundColor: '#0000' }} />
                        <TouchableOpacity onPress={() => this.setState({ filterModalVisible: false })} activeOpacity={1} style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 20, justifyContent: 'center' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ ...shadow, borderRadius: 8, overflow: 'scroll', }}>
                                    <View style={{ backgroundColor: color.primeColor_op_20, height: 35, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 7, borderTopRightRadius: 7 }}>
                                        <Text style={{ fontFamily: font.bold, fontSize: 16, color: color.primeColor }}>Filter</Text>
                                        <TouchableOpacity onPress={() => this.setState({ filterModalVisible: false })} style={{ position: 'absolute', height: 35, width: 35, right: 0, justifyContent: 'center', alignItems: 'center', }}>
                                            <Image source={require('../assets/img/cancle_icon.png')} style={{ tintColor: '#000', height: '40%', width: '40%', resizeMode: 'contain' }} />
                                        </TouchableOpacity>
                                    </View>
                                    <ScrollView bounces={false} contentContainerStyle={{ paddingVertical: 15 }}>
                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="words"
                                                label="Company Name"
                                                value={c_name}
                                                onChangeText={c_name => this.setState({ c_name })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="words"
                                                label="Owner Name"
                                                value={o_name}
                                                onChangeText={o_name => this.setState({ o_name })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                keyboardType="number-pad"
                                                label="Owner Mobile"
                                                maxLength={10}
                                                value={o_mobile}
                                                onChangeText={o_mobile => this.setState({ o_mobile })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                autoCapitalize="words"
                                                label="Name"
                                                value={name}
                                                onChangeText={name => this.setState({ name })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15 }}>
                                            <FloatingLabelInput
                                                keyboardType="number-pad"
                                                label="Mobile"
                                                maxLength={10}
                                                value={mobile}
                                                onChangeText={mobile => this.setState({ mobile })}
                                            />
                                        </View>

                                        <Picker
                                            placeholder="Products/Services"
                                            mainViewStyle={{ marginBottom: 15, }}
                                            data={products_servicesList}
                                            searchBar
                                            value={products_services.name}
                                            onSelect={products_services => this.setState({ products_services })}
                                        />

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <Picker
                                                placeholder="Monitor By"
                                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                                data={employeeList}
                                                searchBar
                                                nameKey="employee_name"
                                                value={monitor_by.employee_name}
                                                onSelect={monitor_by => this.setState({ monitor_by })}
                                            />
                                            <Picker
                                                placeholder="Assign To"
                                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                                data={employeeList}
                                                searchBar
                                                nameKey="employee_name"
                                                value={assign_to.employee_name}
                                                onSelect={assign_to => this.setState({ assign_to })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <Picker
                                                placeholder="Generated By"
                                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                                data={employeeList}
                                                searchBar
                                                nameKey="employee_name"
                                                value={generated_by.employee_name}
                                                onSelect={generated_by => this.setState({ generated_by })}
                                            />
                                            <Picker
                                                placeholder="Lead Source"
                                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                                data={leadSourceList}
                                                searchBar
                                                value={lead_source.name}
                                                onSelect={lead_source => this.setState({ lead_source })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <Picker
                                                placeholder="Country"
                                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                                data={countryList}
                                                searchBar
                                                value={country.name}
                                                onSelect={_country => _country.id != country.id && this.setState({ country: _country, status: [], city: [] }, () => this.stateList(_country))}
                                            />
                                            <Picker
                                                placeholder="State"
                                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                                data={stateList}
                                                searchBar
                                                nameKey="sname"
                                                value={state.sname}
                                                onSelect={_state => _state.id != state.id && this.setState({ state: _state, city: [] }, () => this.cityList(_state))}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <Picker
                                                placeholder="City"
                                                mainViewStyle={{ marginRight: 15, flex: 1, }}
                                                data={cityList}
                                                searchBar
                                                nameKey="cname"
                                                value={city.cname}
                                                onSelect={city => this.setState({ city })}
                                            />
                                            <Picker
                                                placeholder="Lead Status"
                                                mainViewStyle={{ marginLeft: 0, flex: 1, }}
                                                data={leadStatusList}
                                                searchBar
                                                value={lead_status.name}
                                                onSelect={lead_status => this.setState({ lead_status })}
                                            />
                                        </View>

                                        <View style={{ marginBottom: 15, flexDirection: 'row' }}>
                                            <DatePicker
                                                mainViewStyle={{ flex: 1, marginRight: 15 }}
                                                placeholder="Form Date"
                                                value={from_date}
                                                onChange={from_date => this.setState({ from_date })} />
                                            <DatePicker
                                                mainViewStyle={{ flex: 1, marginLeft: 0 }}
                                                placeholder="To Date"
                                                value={to_date}
                                                onChange={to_date => this.setState({ to_date })} />
                                        </View>

                                        <View style={{ marginVertical: 10, marginHorizontal: 15, flexDirection: 'row', alignItems: 'center' }}>
                                            <TouchableOpacity onPress={() => this.setState({ isLoading: true, filterModalVisible: false, page: 1, totalPage: 1, isFilter: true }, () => this.filterLeadList())} style={{ flex: 1, height: 35, justifyContent: 'center', alignItems: 'center', backgroundColor: color.primeColor, borderRadius: 8, }}>
                                                <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>SUBMIT</Text>
                                            </TouchableOpacity>

                                            {isFilter && <TouchableOpacity onPress={() => this.setState({ isLoading: true, filterModalVisible: false, page: 1, totalPage: 1, isFilter: false }, () => { this.clean(); this.leadList(); })} style={{ marginLeft: 15, height: 35, paddingHorizontal: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000', borderRadius: 8, }}>
                                                <Text style={{ fontFamily: font.bold, color: '#fff', fontSize: 16 }}>CLEAR</Text>
                                            </TouchableOpacity>}
                                        </View>
                                    </ScrollView>
                                </View>
                            </TouchableWithoutFeedback>
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                </Modal>
                <CustomAlert ref="CustomAlert" onPressOk={data => this.delete(data)} cancel />
            </View>
        );
    }
}
