import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, ActivityIndicator, RefreshControl, Linking } from 'react-native';
import moment from 'moment';
import { color, font, shadow } from '../Component/Styles';
import CustomAlert from '../Component/CustomAlert';
import { API } from '../Privet';
import Global from '../Global';

export default class Customers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            customersList: [],
            active: null,
        };
    }

    componentDidMount() {
        this.customersList();
    }

    customersList() {
        this.setState({ isLoading: true });
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
        }

        fetch(API.get_all_customers, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                console.log(res);
                if (res.status) {
                    this.setState({ isLoading: false, customersList: res.data, })
                }
                else {
                    this.setState({ isLoading: false });
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    delete(_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            customer_id: _id
        }

        // console.log(data)

        fetch(API.delete_customer, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                if (res.status) {
                    this.customersList();
                } else {
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    render() {
        const { isLoading, customersList, active } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>Customers</Text>
                </View>

                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingTop: 15 }}
                    data={customersList}
                    renderItem={({ item, index }) =>
                        <View key={index + ''} onPress={() => this.props.navigation.navigate('_LeadDetails', { id: item.id })} style={{ ...shadow, marginBottom: 15, paddingHorizontal: 15, paddingTop: 15, borderRadius: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                <View style={{ height: 55, width: 55, backgroundColor: color.primeColor_op_10, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: font.bold, color: color.primeColor }}>{item.company.charAt(0).toUpperCase()}</Text>
                                </View>
                                <View style={{ flex: 1, paddingHorizontal: 12 }}>
                                    <Text numberOfLines={2} style={{ fontFamily: font.bold, fontSize: 15 }}>{item.company}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, fontSize: 13 }}>{item.owner_name}</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.refs.CustomAlert.onShow('Are you sure you want to delete this customers?', '', item.id)} style={{ height: 28, width: 28, borderRadius: 100, backgroundColor: color.borderColor, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image source={require('../assets/img/delete.png')} style={{ height: '45%', width: '45%', resizeMode: 'contain', tintColor: color.lableColor }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ marginTop: 4 }}>
                                <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, }}><Text style={{ color: color.lableColor }}>Mobile No.: </Text> {item.owner_mobile}</Text>
                                <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, }}><Text style={{ color: color.lableColor }}>Email ID: </Text> {item.owner_email}</Text>
                                {item.website ? <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, }}><Text style={{ color: color.lableColor }}>Website: </Text> {item.website}</Text> : null}
                                <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, }}><Text style={{ color: color.lableColor }}>Address: </Text> {item.address}</Text>
                                <Text style={{ fontFamily: font.reg, color: color.lableColor, fontSize: 11.5, marginTop: 3, }}>{moment(item.time_stamp).format('DD-MM-YYYY')} at {moment(item.time_stamp).format('LT')}</Text>
                            </View>

                            <TouchableOpacity onPress={() => this.setState({ active: active == index ? null : index })} style={{ paddingVertical: 12, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: font.bold, color: color.primeColor, fontSize: 14, flex: 1, }}>Contact Person</Text>
                                <Image source={require('../assets/img/back.png')} style={{ height: 14, width: 14, tintColor: color.lableColor, transform: [{ rotate: active == index ? '90deg' : '-90deg' }] }} />
                            </TouchableOpacity>

                            {active == index ? item.contact_person.length != 0 ? item.contact_person.map((data, i) =>
                                <View key={i + ''} style={{ paddingVertical: 0 == i ? 0 : 12, borderBottomWidth: item.contact_person.length - 1 == i ? 0 : 1, paddingBottom: 12 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ flex: 1, fontFamily: font.bold }}>{data.cperson_name}</Text>
                                        {data.cperson_email ? <TouchableOpacity onPress={() => Linking.openURL('mailto:' + data.cperson_email)} style={{ marginRight: 15 }}>
                                            <Image source={require('../assets/img/email.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                        </TouchableOpacity> : null}
                                        <TouchableOpacity onPress={() => Linking.openURL('tel:' + data.cperson_mobile)} >
                                            <Image source={require('../assets/img/phone-call.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                        </TouchableOpacity>
                                    </View>

                                    {data.cperson_email ? <Text style={{ fontFamily: font.reg, marginBottom: 5 }}>{data.cperson_email}</Text> : null}
                                    <Text style={{ fontFamily: font.reg, }}>{data.cperson_mobile}</Text>

                                </View>
                            ) :
                                <View style={{ marginBottom: 15 }}>
                                    <Text style={{ fontFamily: font.bold, fontSize: 12, textAlign: 'center', color: color.lableColor }}>No contact person available!</Text>
                                </View>
                                : null}
                        </View>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Customers not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.customersList()} />}
                />
                <CustomAlert ref="CustomAlert" onPressOk={data => this.delete(data)} cancel />
            </View>
        );
    }
}
