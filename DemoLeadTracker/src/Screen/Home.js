import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, ActivityIndicator, RefreshControl, Dimensions } from 'react-native';
import moment from 'moment';
import OneSignal from 'react-native-onesignal';
import QueryString from 'query-string';
import PureChart from 'react-native-pure-chart';
import PieChart from 'react-native-pie-chart';
import { color, font, shadow } from '../Component/Styles';
import DatePicker from '../Component/DatePicker';
import Picker from '../Component/Picker';
import SnackBar from '../Component/SnackBar';
import Global from '../Global';
import { API } from '../Privet';
import AppUpdateAlert from '../Component/AppUpdateAlert';

const { height, width } = Dimensions.get('window');

var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();

export default class Home extends Component {
    constructor(props) {
        super(props);

        // console.log(moment('1-' + month + '-' + year, 'D-M-YYYY').format('DD-MM-YYYY'));

        this.state = {
            isLoading: true,
            from_date: new Date(moment('01-04-' + year, 'DD-MM-YYYY')),
            to_date: new Date(),
            data: [],
            active: 0,
            chart_data1: [],
            chart_data2: [],
            chart_data3: [],
            graphType: 'bar',
            type: 0,

            countList: [],
            colorList: [],
            amountList: [],
            totalCount: 0,
            totalAmount: 0,

            yearList: [],
            year: { id: moment().format('YYYY'), name: moment().format('YYYY') }
        };
    }

    componentDidMount() {
        this.dashboard();
        let yearList = [];
        for (let i = 2020; i <= parseInt(moment().format('YYYY')); i++) {
            yearList.push({ name: i + '' })
        }
        this.setState({ yearList })

        OneSignal.setLogLevel(6, 0);

        OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
            console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
        });

        OneSignal.setNotificationOpenedHandler(async data => {
            console.log("OneSignal: notification opened:", data.notification);
            if (data.notification.additionalData) {
                if (await data.notification.additionalData.link) {
                    let params = await QueryString.parseUrl(await data.notification.additionalData.link);
                    console.log(params);
                    if (params.query.pid === "view_lead_detail") {
                        await this.props.navigation.navigate('LeadDetails', { lead_id: params.query.id })
                        // that.props.navigation.navigate('SuperAdminInquiryDetail', { inquiry_id: params.query.id })
                    }
                }
            }
        });
    }

    dashboard() {
        const { from_date, to_date, year } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isLoading: true });

        const data = {
            user_id: id,
            utype: urole,
            from_date: moment(from_date).format('DD-MM-YYYY'),
            end_date: moment(to_date).format('DD-MM-YYYY'),
            year: year.name + '',
        }

        // console.log(data);

        fetch(API.dashboard, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isLoading: false })
                if (res.status) {
                    let colorList = [];
                    let amountList = [];
                    let countList = [];
                    let totalCount = 0;
                    let totalAmount = 0;
                    for (let i = 0; i < res.data.chart_data2.length; i++) {
                        colorList.push(res.data.chart_data2[i].color);
                        countList.push(res.data.chart_data2[i].LabelValue);
                        amountList.push(res.data.chart_data2[i].LabelValueAmount);
                        totalCount = totalCount + res.data.chart_data2[i].LabelValue;
                        totalAmount = totalAmount + res.data.chart_data2[i].LabelValueAmount;
                    }
                    this.setState({ data: res.data, totalCount, totalAmount, colorList, countList: totalCount ? countList : [], amountList: totalAmount ? amountList : [], chart_data1: res.data.chart_data1, chart_data2: totalAmount || totalCount ? res.data.chart_data2 : [], chart_data3: res.data.chart_data3 })
                }
                else {
                    this.setState({ chart_data1: [], chart_data2: [], chart_data3: [], colorList: [], countList: [], amountList: [], totalCount: 0, totalAmount: 0 })
                    this.refs.SnackBar.show(res.message);
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    totalLead() {
        const { data } = this.state;
        if (data.lead_status) {
            console.log(data.lead_status.total_order_received_amount, data.lead_status.total_0_approx_amount, data.lead_status.total_1_approx_amount, data.lead_status.total_2_approx_amount, data.lead_status.total_order_loss_amount)
            const total = parseInt(data.lead_status.total_order_received_amount.replace(',', '')) + parseInt(data.lead_status.total_0_approx_amount.replace(',', '')) + parseInt(data.lead_status.total_1_approx_amount.replace(',', '')) + parseInt(data.lead_status.total_2_approx_amount.replace(',', '')) - parseInt(data.lead_status.total_order_loss_amount.replace(',', ''))
            return total;
        }
    }

    render() {
        const { isLoading, from_date, to_date, data, active, chart_data1, chart_data2, chart_data3, graphType, type, colorList, countList, amountList, totalCount, totalAmount, yearList, year } = this.state;

        return (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <SafeAreaView style={{ backgroundColor: color.primeColor }} />
                <StatusBar backgroundColor={color.primeColor} barStyle="light-content" />
                {/* <View style={{ backgroundColor: color.primeColor, height: 180, width: '100%', position: 'absolute' }} /> */}
                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: color.primeColor, }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 22, width: 22, resizeMode: 'contain', tintColor: '#fff' }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 20, paddingHorizontal: 5, flex: 1, color: '#fff' }}>Dashboard</Text>
                </View>

                <View style={{ paddingVertical: '3%', justifyContent: 'center' }}>
                    <View style={{ top: 0, position: 'absolute', backgroundColor: color.primeColor, height: '70%', width: '100%' }} />
                    {active ?
                        <View style={{ ...shadow, marginHorizontal: 15, paddingVertical: 8, borderRadius: 7 }}>
                            <Picker
                                mainViewStyle={{ marginBottom: 8, }}
                                placeholder="Select year"
                                data={yearList}
                                value={year.name}
                                onSelect={year => this.setState({ year }, () => this.dashboard())}
                            />
                        </View>
                        : <View style={{ ...shadow, flexDirection: 'row', marginHorizontal: 15, paddingVertical: 8, borderRadius: 7 }}>
                            <DatePicker
                                mainViewStyle={{ flex: 1, marginBottom: 8, }}
                                placeholder="From Date"
                                maxDate={to_date}
                                value={from_date}
                                onChange={from_date => this.setState({ from_date }, () => this.dashboard())} />

                            <DatePicker
                                mainViewStyle={{ flex: 1, marginBottom: 8, marginLeft: 0 }}
                                placeholder="To Date"
                                minDate={from_date}
                                maxDate={new Date()}
                                value={to_date}
                                onChange={to_date => this.setState({ to_date }, () => this.dashboard())} />
                        </View>}
                </View>

                <ScrollView style={{ flexGrow: 1, }}
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.setState({ from_date: new Date(moment('01-04-' + year, 'DD-MM-YYYY')), to_date: new Date(), isLoading: true, year: { id: moment().format('YYYY'), name: moment().format('YYYY') } }, () => this.dashboard())} />} >

                    {active ?
                        <>
                            <View style={{ flexDirection: 'row', alignItems: 'center', margin: 15 }}>
                                <View borderColor={color.primeColor} style={{ flex: 1, borderWidth: 1.5, paddingVertical: 5, borderTopLeftRadius: 50, borderBottomLeftRadius: 50, backgroundColor: graphType === 'line' ? color.primeColor : '#fff' }}>
                                    <TouchableOpacity onPress={() => this.setState({ graphType: 'line' })} style={{ flex: 1, }}>
                                        <Text style={{ textAlign: 'center', fontSize: 11.5, fontFamily: font.bold, color: color.primeColor, color: graphType === 'line' ? '#fff' : color.primeColor }}>LINE</Text>
                                    </TouchableOpacity>
                                </View>
                                <View borderColor={color.primeColor} style={{ flex: 1, borderWidth: 1.5, paddingVertical: 5, borderTopRightRadius: 50, borderBottomRightRadius: 50, marginRight: 20, backgroundColor: graphType === 'bar' ? color.primeColor : '#fff' }}>
                                    <TouchableOpacity onPress={() => this.setState({ graphType: 'bar' })} style={{ flex: 1, }}>
                                        <Text style={{ textAlign: 'center', fontSize: 11.5, fontFamily: font.bold, color: color.primeColor, color: graphType === 'bar' ? '#fff' : color.primeColor }}>BAR</Text>
                                    </TouchableOpacity>
                                </View>

                                <View borderColor={color.primeColor} style={{ flex: 1, borderWidth: 1.5, paddingVertical: 5, borderTopLeftRadius: 50, borderBottomLeftRadius: 50, backgroundColor: !type ? color.primeColor : '#fff' }}>
                                    <TouchableOpacity onPress={() => this.setState({ type: 0 })} style={{ flex: 1, }}>
                                        <Text style={{ textAlign: 'center', fontSize: 11.5, fontFamily: font.bold, color: color.primeColor, color: !type ? '#fff' : color.primeColor }}>COUNT</Text>
                                    </TouchableOpacity>
                                </View>
                                <View borderColor={color.primeColor} style={{ flex: 1, borderWidth: 1.5, paddingVertical: 5, borderTopRightRadius: 50, borderBottomRightRadius: 50, backgroundColor: type ? color.primeColor : '#fff' }}>
                                    <TouchableOpacity onPress={() => this.setState({ type: 1 })} style={{ flex: 1, }}>
                                        <Text style={{ textAlign: 'center', fontSize: 11.5, fontFamily: font.bold, color: color.primeColor, color: type ? '#fff' : color.primeColor }}>AMOUNT</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {chart_data1.length != 0 ? <View style={{ ...shadow, margin: 15, borderRadius: 7, paddingTop: 15, paddingLeft: 5, flexDirection: 'row', alignItems: 'center', }}>
                                <PureChart
                                    data={type ? chart_data3 : chart_data1}
                                    type={graphType}
                                    height={width / 1.8}
                                    // width={50}
                                    width={'95%'}
                                // showEvenNumberXaxisLabel={true}
                                />
                            </View> : null}

                            {chart_data2.length != 0 ? <View style={{ ...shadow, margin: 15, borderRadius: 7, padding: 15, flexDirection: 'row', alignItems: 'center' }}>
                                <PieChart
                                    chart_wh={width * .4}
                                    series={type ? amountList : countList}
                                    // series={[]}
                                    sliceColor={colorList}
                                />
                                <View style={{ flex: 1, marginLeft: 20 }}>
                                    {chart_data2.map((item, index) =>
                                        <View style={{ flexDirection: 'row', marginBottom: chart_data2.length - 1 == index ? 0 : 15, alignItems: 'center' }}>
                                            <View style={{ width: 15, backgroundColor: item.color, height: 8, borderRadius: 10 }} />
                                            <Text style={{ flex: 1, fontFamily: font.bold, fontSize: 11, marginLeft: 10 }}>{item.LabelName} ({type ? Math.round(item.LabelValueAmount * 100 / totalAmount) : Math.round(item.LabelValue * 100 / totalCount)}%)</Text>
                                        </View>
                                    )}
                                </View>
                            </View> : null}
                        </>
                        : <>
                            <View style={{ ...shadow, margin: 15, borderRadius: 7, marginBottom: 20 }}>
                                <View style={{ flexDirection: 'row', borderBottomWidth: 1 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead')} style={{ flex: 1, padding: 15, borderRightWidth: 1 }}>
                                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.primeColor }}>{data.total_lead ? data.total_lead : '0'}</Text>}
                                        {!isLoading && <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.total_lead_amount}</Text>}
                                        <Text style={{ fontFamily: font.reg, }}>Total Lead</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '2', name: 'Hot' } })} style={{ flex: 1, padding: 15 }}>
                                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.hot }}>{data.lead_status ? data.lead_status.total_2 : '0'}</Text>}
                                        {!isLoading && <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.lead_status ? data.lead_status.total_2_approx_amount : '0'}</Text>}
                                        <Text style={{ fontFamily: font.reg, }}>Hot Lead</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', borderBottomWidth: 1 }}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '1', name: 'Warm' } })} style={{ flex: 1, padding: 15, borderRightWidth: 1 }}>
                                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.warm }}>{data.lead_status ? data.lead_status.total_1 : '0'}</Text>}
                                        {!isLoading && <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.lead_status ? data.lead_status.total_1_approx_amount : '0'}</Text>}
                                        <Text style={{ fontFamily: font.reg, }}>Warm Lead</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '0', name: 'Cold' } })} style={{ flex: 1, padding: 15 }}>
                                        {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.cold }}>{data.lead_status ? data.lead_status.total_0 : '0'}</Text>}
                                        {!isLoading && <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.lead_status ? data.lead_status.total_0_approx_amount : '0'}</Text>}
                                        <Text style={{ fontFamily: font.reg, }}>Cold Lead</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* <View style={{ flexDirection: 'row', paddingHorizontal: 15, marginBottom: 20 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '4', name: 'Order Loss' } })} style={{ flex: 1, ...shadow, borderRadius: 5, marginRight: 20, padding: 15 }}>
                            {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.red }}>{data.lead_status ? data.lead_status.total_4 : '0'}</Text>}
                            <Text style={{ fontFamily: font.reg, }}>Order Loss</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '3', name: 'Order Received' } })} style={{ flex: 1, ...shadow, borderRadius: 5, padding: 15 }}>
                            {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.green }}>{data.lead_status ? data.lead_status.total_3 : '0'}</Text>}
                            <Text style={{ fontFamily: font.reg, }}>Order Received</Text>
                        </TouchableOpacity>
                    </View> */}

                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '4', name: 'Order Loss' } })} style={{ ...shadow, borderRadius: 5, marginHorizontal: 15, marginBottom: 20, padding: 15 }}>
                                {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.red }}>{data.lead_status ? data.lead_status.total_4 : '0'}</Text>}
                                {!isLoading && <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.lead_status ? data.lead_status.total_order_loss_amount : '0'}</Text>}
                                <Text style={{ fontFamily: font.reg, fontSize: 14 }}>Order Loss</Text>
                                <Image source={require('../assets/img/wallet_amount_icon.png')} style={{ position: 'absolute', right: 0, height: 35, width: 35, borderTopRightRadius: 5, tintColor: color.red }} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Lead', { lead_status: { id: '3', name: 'Order Received' } })} style={{ ...shadow, borderRadius: 5, marginHorizontal: 15, marginBottom: 20, padding: 15 }}>
                                {isLoading ? <ActivityIndicator color={color.primeColor} style={{ alignSelf: 'flex-start', marginBottom: 10, }} /> : <Text style={{ fontFamily: font.bold, fontSize: 22, marginBottom: 10, color: color.green }}>{data.lead_status ? data.lead_status.total_3 : '0'}</Text>}
                                <Text style={{ fontFamily: font.bold, fontSize: 13 }}>Rs. {data.lead_status ? data.lead_status.total_order_received_amount : '0'}</Text>
                                <Text style={{ fontFamily: font.reg, fontSize: 14 }}>Order Received</Text>
                                <Image source={require('../assets/img/wallet_amount_icon.png')} style={{ position: 'absolute', right: 0, height: 35, width: 35, borderTopRightRadius: 5, tintColor: color.green }} />
                            </TouchableOpacity>
                        </>}

                    <View style={{ paddingBottom: 80 }} />
                </ScrollView>

                <TouchableOpacity onPress={() => this.setState({ active: !active })} style={{ ...shadow, height: 55, width: 55, borderRadius: 55, position: 'absolute', bottom: 20, right: 20, justifyContent: 'center', alignItems: 'center' }}>
                    {active ? <Image source={require('../assets/img/list.png')} style={{ height: '55%', width: '55%', resizeMode: 'contain' }} />
                        : <Image source={require('../assets/img/graph.png')} style={{ height: '75%', width: '75%', resizeMode: 'contain' }} />}
                </TouchableOpacity>

                <SnackBar ref="SnackBar" />
                <AppUpdateAlert />
            </View >
        );
    }
}
