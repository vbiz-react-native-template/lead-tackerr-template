import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, RefreshControl, Linking, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import Picker from '../Component/Picker';
import DatePicker from '../Component/DatePicker';
import { API } from '../Privet';
import Global from '../Global';

export default class PendingFollowUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            totalPage: 1,
            page: 1,
            leadList: [],
        };
    }

    componentDidMount() {
        this.leadList();
    }

    // lead List
    leadList() {
        const { isLoading, leadList, totalPage, page } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isFilter: false, });

        if (totalPage >= page) {
            const data = {
                user_id: id,
                utype: urole,
                page: page
            }

            // console.log(data);

            fetch(API.pending_followup, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then((response) => response.json())
                .then((resp) => {
                    const res = resp.response[0];
                    // console.log(res);
                    if (res.status) {
                        if (isLoading || leadList.length == 0) {
                            this.setState({ isLoading: false, leadList: res.data, totalPage: res.totalpage, page: page + 1 })
                        } else {
                            this.setState({ isLoading: false, leadList: [...leadList, ...res.data], totalPage: res.totalpage, page: page + 1 })
                        }
                    }
                    else {
                        this.setState({ isLoading: false });
                        this.refs.SnackBar.show(res.message);
                    }
                })
                .catch((error) => {
                    this.setState({ isLoading: false });
                    console.log('error: ', error);
                });
        }
    }

    renderFooter = () => {
        const { totalPage, page, isLoading, leadList } = this.state;
        if (page <= totalPage && !isLoading && leadList.length != 0) {

            return (
                <View>
                    <ActivityIndicator color={color.primeColor} style={{ margin: 20 }} />
                </View>
            )
        } else {
            return null
        }
    }

    render() {
        const { isLoading, leadList, } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/menu.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>Pending Follow Up</Text>
                </View>

                <FlatList
                    contentContainerStyle={{ flexGrow: 1, paddingHorizontal: 15, paddingTop: 15 }}
                    data={leadList}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity key={index + ''} onPress={() => this.props.navigation.navigate('LeadDetails', { lead_id: item.lead_id })} style={{ ...shadow, marginBottom: 15, padding: 15, borderRadius: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                <View style={{ height: 55, width: 55, backgroundColor: color.red + '11', borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 30, fontFamily: font.bold, color: color.red }}>{item.company.charAt(0).toUpperCase()}</Text>
                                </View>
                                <View style={{ flex: 1, paddingHorizontal: 12 }}>
                                    <Text numberOfLines={2} style={{ fontFamily: font.bold, fontSize: 15 }}>{item.company}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, fontSize: 13 }}>{item.cperson_email}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: font.reg, marginTop: 3, fontSize: 13 }}>{item.cperson_mobile}</Text>
                                </View>

                                <View>
                                    {item.cperson_email ? <TouchableOpacity onPress={() => Linking.openURL('mailto:' + item.cperson_email)} style={{ marginBottom: 10 }}>
                                        <Image source={require('../assets/img/email.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                    </TouchableOpacity> : null}
                                    <TouchableOpacity onPress={() => Linking.openURL('tel:' + item.cperson_mobile)} >
                                        <Image source={require('../assets/img/phone-call.png')} style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: color.primeColor }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ marginTop: 12, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Ex. Name:  </Text>
                                <Text style={{ fontFamily: font.reg, }}>{item.exname}</Text>
                            </View>
                            <View style={{ marginTop: 12, flexDirection: 'row' }}>
                                <View style={{ flex: 1, }}>
                                    <Text style={{ fontFamily: font.reg, marginBottom: 6 }}>{item.nfdate}</Text>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Next Date</Text>
                                </View>
                                {/* <View style={{ height: '100%', width: 1.5, backgroundColor: color.borderColor }} /> */}
                                <View style={{ flex: 1, }}>
                                    <Text style={{ fontFamily: font.reg, marginBottom: 6 }}>{item.nftime}</Text>
                                    <Text style={{ fontFamily: font.reg, color: color.lableColor }}>Next Time</Text>
                                </View>
                            </View>

                            <Text style={{ fontFamily: font.reg, color: color.lableColor, marginTop: 12 }}>Remarks</Text>
                            <Text style={{ marginTop: 2, fontFamily: font.reg }}>{item.remarks}</Text>

                            <View style={{ marginTop: 12, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ fontFamily: font.reg, color: color.lableColor, fontSize: 13, flex: 1, }}>{item.fdate} {item.ftime}</Text>
                                <Image source={require('../assets/img/back.png')} style={{ height: 14, width: 14, tintColor: color.lableColor, transform: [{ rotate: '180deg' }] }} />
                            </View>
                        </TouchableOpacity>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Lead not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.setState({ isLoading: true, page: 1, totalPage: 1, }, () => this.leadList())} />}
                    onEndReached={() => this.leadList()}
                    onEndReachedThreshold={0.1}
                    ListFooterComponent={this.renderFooter}
                />

            </View>
        );
    }
}
