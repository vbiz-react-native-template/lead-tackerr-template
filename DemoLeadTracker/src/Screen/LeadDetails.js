import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, ScrollView, FlatList, Modal, KeyboardAvoidingView, Platform, TouchableWithoutFeedback } from 'react-native';
import moment from 'moment';
import { color, font, shadow } from '../Component/Styles';
import FloatingLabelInput from '../Component/FloatingLabelInput';
import Picker from '../Component/Picker';
import DatePicker from '../Component/DatePicker';
import BasicDetails from './LeadDetailsTab/BasicDetails';
import FollowUp from './LeadDetailsTab/FollowUp';
import Meeting from './LeadDetailsTab/Meeting';
import Quatations from './LeadDetailsTab/Quatations';
import { API } from '../Privet';
import Global from '../Global';

export default class LeadDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            active: '1',
            lead_id: '',
            leadDetails: [],
        };
    }
    componentDidMount() {
        const { navigation, route } = this.props;
        // console.log(route.params.lead_id);
        // const lead_id = navigation.getParam('lead_id','');
        this.setState({ lead_id: route.params.lead_id }, () => this.leadDetails())
    }

    leadDetails() {
        const { lead_id } = this.state;
        const { id, urole } = Global.main.state;

        this.setState({ isLoading: true });
        const data = {
            user_id: id,
            utype: urole,
            lead_id: lead_id,
        }

        // console.log(data);

        fetch(API.lead_detail, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                this.setState({ isLoading: false });
                if (res.status) {
                    this.setState({ leadDetails: res.data });
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    render() {
        const { isLoading, active, leadDetails, lead_id } = this.state;
        const { lead_contacts = [], lead_followup = [], lead_meeting = [], lead_quotation = [] } = leadDetails;
        return (
            <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/back.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>Lead Details</Text>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddLead', { item: leadDetails })} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/edit.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, }}>
                    <View>
                        <ScrollView horizontal contentContainerStyle={{ paddingVertical: 10, paddingLeft: 15 }} >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => this.setState({ active: '1' })} style={{ marginRight: 20 }}>
                                    <Text style={{ fontFamily: font.bold, color: active === '1' ? '#000' : color.lableColor, fontSize: 15 }}>Basic Details</Text>
                                    <View style={{ height: 2, width: '40%', backgroundColor: active === '1' ? '#000' : '#0000', marginTop: 10 }} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({ active: '2' })} style={{ marginRight: 20 }}>
                                    <Text style={{ fontFamily: font.bold, color: active === '2' ? '#000' : color.lableColor, fontSize: 15 }}>Follow Up</Text>
                                    <View style={{ height: 2, width: '40%', backgroundColor: active === '2' ? '#000' : '#0000', marginTop: 10 }} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({ active: '3' })} style={{ marginRight: 20 }}>
                                    <Text style={{ fontFamily: font.bold, color: active === '3' ? '#000' : color.lableColor, fontSize: 15 }}>Meeting</Text>
                                    <View style={{ height: 2, width: '40%', backgroundColor: active === '3' ? '#000' : '#0000', marginTop: 10 }} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.setState({ active: '4' })} style={{ marginRight: 20 }}>
                                    <Text style={{ fontFamily: font.bold, color: active === '4' ? '#000' : color.lableColor, fontSize: 15 }}>Quotations</Text>
                                    <View style={{ height: 2, width: '40%', backgroundColor: active === '4' ? '#000' : '#0000', marginTop: 10 }} />
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>

                    {active === '1' && <BasicDetails data={leadDetails} lead_id={lead_id} isLoading={isLoading} onRefresh={() => this.leadDetails()} />}
                    {active === '2' && <FollowUp data={lead_followup} lead_id={lead_id} isLoading={isLoading} onRefresh={() => this.leadDetails()} />}
                    {active === '3' && <Meeting data={lead_meeting} lead_id={lead_id} isLoading={isLoading} onRefresh={() => this.leadDetails()} />}
                    {active === '4' && <Quatations data={lead_quotation} lead_id={lead_id} isLoading={isLoading} onRefresh={() => this.leadDetails()} />}

                </View>

            </View>
        );
    }
}
