import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, StatusBar, FlatList, ScrollView, ActivityIndicator, RefreshControl } from 'react-native';
import moment from 'moment';
import QueryString from 'query-string';
import { color, font, shadow } from '../Component/Styles';
import Global from '../Global';
import { API } from '../Privet';

export default class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            notificationList: [],
        };
    }

    componentDidMount() {
        this.notificationList();
    }

    notificationList() {
        this.setState({ isLoading: true });
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
        }

        fetch(API.notification, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
                const res = resp.response[0];
                // console.log(res);
                if (res.status) {
                    this.setState({ isLoading: false, notificationList: res.data, })
                }
                else {
                    this.setState({ isLoading: false });
                }
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log('error: ', error);
            });
    }

    openNotification(item, index) {
        const { notificationList } = this.state;
        if (item.status === '0') {
            notificationList[index].status = '1';
            this.setState({ notificationList });
            this.notificationStatusUpdate(item.id)
        }

        let params = QueryString.parseUrl(item.nurl);
        console.log(params);
        if (params.query.pid === "view_lead_detail") {
            this.props.navigation.navigate('LeadDetails', { lead_id: params.query.id })
        }
    }

    notificationStatusUpdate(_id) {
        const { id, urole } = Global.main.state;
        const data = {
            user_id: id,
            utype: urole,
            notification_id: _id
        }

        fetch(API.notification_update, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then((response) => response.json())
            .then((resp) => {
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    render() {
        const { isLoading, notificationList } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#F3F3F3' }}>
                <SafeAreaView />
                <StatusBar backgroundColor={'#F3F3F3'} barStyle="dark-content" />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ paddingHorizontal: 15, paddingVertical: 10 }}>
                        <Image source={require('../assets/img/back.png')} style={{ height: 20, width: 20, resizeMode: 'contain', tintColor: color.primeColor }} />
                    </TouchableOpacity>
                    <Text style={{ fontFamily: font.bold, fontSize: 19, paddingHorizontal: 5, flex: 1, color: color.primeColor }}>Notification</Text>
                </View>

                <FlatList
                    contentContainerStyle={{ padding: 15, flexGrow: 1 }}
                    data={notificationList}
                    renderItem={({ item, index }) =>
                        <TouchableOpacity key={index + ''} onPress={() => this.openNotification(item, index)} style={{ ...shadow, borderRadius: 6, marginBottom: 15, flexDirection: 'row', }}>
                            <View style={{ height: 80, width: 65, borderTopLeftRadius: 7, borderBottomLeftRadius: 7, backgroundColor: item.status === '1' ? color.borderColor : color.primeColor_op_10, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/img/notification.png')} style={{ height: '45%', width: '40%', resizeMode: 'contain', tintColor: item.status === '1' ? color.lableColor : color.primeColor }} />
                            </View>
                            <Text numberOfLines={3} style={{ flex: 1, fontFamily: font.reg, padding: 12, color: item.status === '1' ? color.lableColor : '#000' }}>{item.msg}</Text>
                        </TouchableOpacity>
                    }
                    ListEmptyComponent={
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            {!isLoading ? <Text style={{ fontFamily: font.bold, textAlign: 'center' }}>Notification not found!</Text> :
                                Platform.OS === 'ios' ?
                                    <ActivityIndicator color={color.primeColor} size="large" /> : null
                            }
                        </View>
                    }
                    refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => this.notificationList()} />}
                />

            </View>
        );
    }
}
